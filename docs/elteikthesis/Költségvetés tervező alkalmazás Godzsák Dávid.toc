\select@language {hungarian} \boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {chapter}{\numberline {1}Bevezet\IeC {\'e}s}{1}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {1.1}P\IeC {\'a}r sz\IeC {\'o} a JavaScriptr\IeC {\H o}l}{1}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {1.2}A feladat le\IeC {\'\i }r\IeC {\'a}sa}{2}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {1.3}A t\IeC {\'e}mav\IeC {\'a}laszt\IeC {\'a}s indokl\IeC {\'a}sa}{3}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {part}{I.\hspace {1em}Felhaszn\IeC {\'a}l\IeC {\'o}i dokument\IeC {\'a}ci\IeC {\'o}}{5}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {chapter}{\numberline {2}Szoftver bemutat\IeC {\'a}sa}{7}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {chapter}{\numberline {3}Els\IeC {\H o} l\IeC {\'e}p\IeC {\'e}sek}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {3.1}A haszn\IeC {\'a}lathoz sz\IeC {\"u}ks\IeC {\'e}ges el\IeC {\H o}felt\IeC {\'e}telek}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {3.2}Telep\IeC {\'\i }t\IeC {\'e}s}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Windowson}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Mac}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Linuxos}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Android}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{iOS}{9}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {chapter}{\numberline {4}Az alkalmaz\IeC {\'a}s haszn\IeC {\'a}lata}{11}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {4.1}\IeC {\'A}ltal\IeC {\'a}nos m\IeC {\H u}veletek}{11}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.1.1}Regisztr\IeC {\'a}ci\IeC {\'o}}{11}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.1.2}Men\IeC {\"u} kezel\IeC {\'e}se}{11}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.1.3}Kijelentkez\IeC {\'e}s}{11}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.1.4}Bejelentkez\IeC {\'e}s}{12}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {4.2}Tranzakci\IeC {\'o}k kezel\IeC {\'e}se}{12}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.2.1}Tranzakci\IeC {\'o}k list\IeC {\'a}z\IeC {\'a}sa}{12}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.2.2}Tranzakci\IeC {\'o} l\IeC {\'e}trehoz\IeC {\'a}sa}{13}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.2.3}Tranzakci\IeC {\'o} m\IeC {\'o}dos\IeC {\'\i }t\IeC {\'a}sa}{14}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.2.4}Tranzakci\IeC {\'o} t\IeC {\"o}rl\IeC {\'e}se}{15}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.2.5}Off-line m\IeC {\'o}d}{15}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.2.6}Kateg\IeC {\'o}ri\IeC {\'a}k k\IeC {\'e}sz\IeC {\'\i }t\IeC {\'e}se}{15}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {4.3}Ism\IeC {\'e}tl\IeC {\H o}d\IeC {\H o} tranzakci\IeC {\'o}k}{16}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {4.4}Statisztik\IeC {\'a}k}{17}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {4.5}Sp\IeC {\'o}rol\IeC {\'a}sok}{17}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.5.1}Sp\IeC {\'o}rol\IeC {\'a}s l\IeC {\'e}trehoz\IeC {\'a}sa}{18}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.5.2}Sp\IeC {\'o}rol\IeC {\'a}sok megtekint\IeC {\'e}se, m\IeC {\'o}dos\IeC {\'\i }t\IeC {\'a}sa}{18}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {4.5.3}Szab\IeC {\'a}lyszer\IeC {\H u}s\IeC {\'e}gek}{19}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {4.6}Csoportok}{20}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {part}{II.\hspace {1em}Fejleszt\IeC {\H o}i dokument\IeC {\'a}ci\IeC {\'o}}{23}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {chapter}{\numberline {5}Specifik\IeC {\'a}ci\IeC {\'o}}{25}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.1}Feladat}{25}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.2}Feladat elemz\IeC {\'e}se}{25}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.3}Eszk\IeC {\"o}z\IeC {\"o}k}{27}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Angular\cite {angular_docs}}{27}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Ionic\cite {ionic_docs}}{28}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Electron\cite {electron_docs}}{28}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Cordova\cite {cordova_docs}}{29}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Express\cite {express_docs}}{29}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{JWT\cite {jwt_docs}}{29}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Passport\cite {passport_docs}}{29}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{BCrypt\cite {bcrypt_docs}}{30}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Mongoose\cite {mongoose_docs}}{30}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Node Schedule\cite {node_schedule_docs}}{30}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Jasmine\cite {jasmine_docs}, Karma\cite {karma_docs}, Mocha\cite {mocha_docs}, Chai\cite {chai_docs}, Sinon\cite {sinon_docs}}{30}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Lodash\cite {lodash_docs}, Moment\cite {moment_docs}}{30}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Gulp\cite {gulp_docs}}{30}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.4}Adatb\IeC {\'a}zis, s\IeC {\'e}m\IeC {\'a}k}{31}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.4.1}Users}{31}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.4.2}Transactions}{31}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.4.3}RepeatingTransactions}{32}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.4.4}Group}{33}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.4.5}Savings}{33}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.5}Szerver}{33}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.5.1}Fel\IeC {\'e}p\IeC {\'\i }t\IeC {\'e}s}{34}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.5.2}Authentik\IeC {\'a}ci\IeC {\'o}}{35}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.5.3}Controllerek}{35}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{User}{35}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Transaction}{36}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Category}{36}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{RepeatingTransaction}{36}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Group}{36}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Saving}{36}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Statistics}{37}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.5.4}Route-ok}{37}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.5.5}Modellek}{37}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Valid\IeC {\'a}l\IeC {\'a}s}{37}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.5.6}Util-ok}{37}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.6}El\IeC {\H o}k\IeC {\'e}sz\IeC {\"u}letek}{39}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.6.1}Eszk\IeC {\"o}z\IeC {\"o}k telep\IeC {\'\i }t\IeC {\'e}se}{39}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.6.2}K\IeC {\"o}rnyezet kialak\IeC {\'\i }t\IeC {\'a}sa}{39}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.7}Fejleszt\IeC {\'e}s menete}{40}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.7.1}K\IeC {\'o}dol\IeC {\'a}si st\IeC {\'\i }lus}{40}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.7.2}Szoftver technol\IeC {\'o}giai megold\IeC {\'a}sok}{41}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {5.8}Elk\IeC {\'e}sz\IeC {\'\i }tend\IeC {\H o} oldalak}{42}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.1}Service-ek}{43}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.2}Authentik\IeC {\'a}ci\IeC {\'o}}{43}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{AuthService}{43}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Bejelentkez\IeC {\'e}s}{43}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Regisztr\IeC {\'a}ci\IeC {\'o}}{43}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.3}Tranzakci\IeC {\'o}k}{45}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{TransactionService}{45}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Lista}{45}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{L\IeC {\'e}trehoz\IeC {\'a}s/m\IeC {\'o}dos\IeC {\'\i }t\IeC {\'a}s}{46}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Ism\IeC {\'e}tl\IeC {\H o}d\IeC {\H o} tranzakci\IeC {\'o}}{46}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.4}Statisztik\IeC {\'a}k}{46}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{StatisticsService}{48}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{K\IeC {\"o}r diagram}{48}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Vonal diagram}{48}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.5}Sp\IeC {\'o}rol\IeC {\'a}s}{49}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Lista}{50}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{L\IeC {\'e}trehoz\IeC {\'a}s}{50}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{R\IeC {\'e}szletek}{50}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.6}Csoport}{50}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.7}Sp\IeC {\'o}rol\IeC {\'a}s mutat\IeC {\'o}}{51}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {5.8.8}Kateg\IeC {\'o}ri\IeC {\'a}k}{51}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {chapter}{\numberline {6}Implement\IeC {\'a}ci\IeC {\'o}}{53}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.1}K\IeC {\"o}rnyezet elind\IeC {\'\i }t\IeC {\'a}sa}{53}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.2}K\IeC {\"o}nyvt\IeC {\'a}rszerkezetek, f\IeC {\H o}bb f\IeC {\'a}jlok a k\IeC {\'e}sz alkalmaz\IeC {\'a}sban}{54}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.2.1}Szerver}{54}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.2.2}Alkalmaz\IeC {\'a}s}{54}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.2.3}Git}{56}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.3}Adatb\IeC {\'a}zis seed-el\IeC {\'e}se}{56}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.4}Gulp}{56}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Copy}{57}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Watch}{57}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Build}{57}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Serve}{58}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Run}{58}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Dist}{58}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.5}Az alkalmaz\IeC {\'a}s}{58}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.5.1}Sp\IeC {\'o}rol\IeC {\'a}sok sz\IeC {\'a}m\IeC {\'\i }t\IeC {\'a}s\IeC {\'a}nak algoritmusa}{58}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.5.2}Fejleszt\IeC {\'e}s sor\IeC {\'a}n alkalmazott v\IeC {\'a}ltoztat\IeC {\'a}sok}{59}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Tranzakci\IeC {\'o}k}{59}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Server Service}{60}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Sync Service}{60}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.6}Adatb\IeC {\'a}zissal kapcsolatos megfontol\IeC {\'a}sok, sk\IeC {\'a}l\IeC {\'a}z\IeC {\'o}d\IeC {\'a}s}{60}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.7}A szerver}{61}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.7.1}Alkalmazott nyelvi elemek}{61}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.7.2}Fejleszt\IeC {\'e}s sor\IeC {\'a}n alkalmazott v\IeC {\'a}ltoztat\IeC {\'a}sok}{62}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Controller}{62}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{OK}{62}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{DATA}{62}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{ERROR}{62}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Tranzakci\IeC {\'o}k}{62}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Node Scheduler}{63}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {6.7.3}Deploy}{63}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {6.8}Tov\IeC {\'a}bbfejleszt\IeC {\'e}si \IeC {\"o}tletek}{64}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Docker}{64}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Social Media Login}{64}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{Soksz\IeC {\'\i }n\IeC {\H u} tranzakci\IeC {\'o} felvitel}{64}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {paragraph}{P\IeC {\'e}nznem}{64}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {chapter}{\numberline {7}Tesztel\IeC {\'e}si terv}{67}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {7.1}Szerver}{67}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.1.1}Router}{67}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.1.2}Controllerek, Modellek}{68}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{TransactionController}{69}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{UserController}{69}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{GroupController}{70}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{StatisticsController}{70}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.1.3}SavingsController}{71}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.1.4}CategoryController}{72}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.1.5}RepeatingTransactionsController}{72}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {section}{\numberline {7.2}Alkalmaz\IeC {\'a}s}{72}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.2.1}Bejelentkez\IeC {\'e}s, Regisztr\IeC {\'a}ci\IeC {\'o}}{72}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Service}{72}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Fel\IeC {\"u}let}{73}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.2.2}Tranzakci\IeC {\'o}k, Ism\IeC {\'e}tl\IeC {\H o}d\IeC {\H o} tranzakci\IeC {\'o}k}{73}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Service}{73}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Fel\IeC {\"u}let}{73}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.2.3}Csoportok}{74}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Service}{74}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Fel\IeC {\"u}let}{74}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.2.4}Statisztik\IeC {\'a}k}{74}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Service}{74}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Fel\IeC {\"u}let}{75}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsection}{\numberline {7.2.5}Sp\IeC {\'o}rol\IeC {\'a}sok}{75}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Service}{75}
\defcounter {refsection}{0}\relax 
\select@language {hungarian} \contentsline {subsubsection}{Fel\IeC {\"u}let}{75}
