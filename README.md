# Budget - Multiplatformos költségvetés-kezelő

Egy alkalmazás mindenki számára: kiadásaik követésére, tervezésére, spórolás elősegítésére.

## Indítás

A dist/ könyvtárban válasszuk az operációs rendszerünknek megfelelő alkönyvtárat, majd az ott található readme.txt-ben leírtak alapján telepítsük.

## Telepítés

Az alkalmazás továbbfejlesztéséhez ki kell alakítanunk a megfelelő fejlesztőkörnyezetet, el kell indítanunk az alkalmazást fejlesztő módban.

Telepítsük fel a következő eszközöket: 

- Node.js
- MongoDB
- Cordova
- Gulp

Az alkalamzás futtatása:

1. Indítsuk el a MongoDB-t: 'sudo mongod'
2. A budget/ és a server/ könyvtárakban adjuk ki az 'npm i' parancsot.
3. Indítsuk el a szervert a server/ mappában kiadott npm run server paranccsal.
4. Futtassuk az alkalmazást a budget/ alkönyvtárban kiadott 'gulp serve -v=ionic' paranccsal
5. Nyissuk meg a build eszköz által meghatározott oldalt a böngészőnkben (alap esetben localhost:8080)

## Dokumentáció

A részletes fejlesztői és felhasználói dokumentációt, a docs/ mappában találjuk. 


Godzsák Dávid

Budapest, 2017



