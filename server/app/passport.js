'use strict';

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.28..
 */

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./models/user');
const config = require('./../config/database');

/**
 * Creates a JwtStrategy for passport
 * Reads token from auth-header, verifies User by ID stored in the token
 * @param passport
 */
module.exports = function (passport) {
    passport.use(new JwtStrategy(opts, verify));
};

const opts = {
    secretOrKey: config.secret,
    jwtFromRequest: ExtractJwt.fromAuthHeader()
};


const verify = (jwtPayload, done) => {
    User.findById(jwtPayload.id)
        .then(
            user => user ? done(null, user) : done(null, false),
            err => done(err, false)
        );
};