'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const Transaction = require('./models/transaction');
const moment = require('moment');


/**
 * Basic categories for seeding the Database, with dummy seed data
 * edit categories object as follows:
 * Add an object with a name of your choice e.q.: food.
 * The object should have the following fields:
 * category : the category of the transaction
 * perMonth : how many transactions there are in a month
 * from     : lower range of price
 * to       : upper range of price
 */
const categories = {
    shopping: {
        category: {color: 'pink', name: 'shopping', subcategory: '', icon: 'basket'},
        perMonth: 10,
        from: -3000,
        to: -5000
    },
    food: {
        category: {color: 'purple', name: 'food', subcategory: '', icon: 'pizza'},
        perMonth: 17,
        from: -1000,
        to: -4000
    },
    rent: {
        category: {color: 'royalblue', name: 'rent', subcategory: '', icon: 'house'},
        perMonth: 1,
        from: -21000,
        to: -21000
    },
    car: {
        category: {color: 'gold', name: 'transport', subcategory: 'car', icon: 'car'},
        perMonth: 0,
        from: -2100,
        to: -10000
    },
    bus: {
        category: {color: 'maroon', name: 'transport', subcategory: 'public', icon: 'bus'},
        perMonth: 1,
        from: -3500,
        to: -4000
    },
    party: {
        category: {color: 'lightcoral', name: 'party', subcategory: '', icon: 'happy'},
        perMonth: 2,
        from: -1500,
        to: -5000
    },
    date: {
        category: {color: 'mediumorchid', name: 'date', subcategory: '', icon: 'wine'},
        perMonth: 3,
        from: -5000,
        to: -20000
    },
    medicine: {
        category: {color: 'lightblue', name: 'health', subcategory: 'medicine', icon: 'medkit'},
        perMonth: 1,
        from: -1000,
        to: -2000
    },
    doctor: {
        category: {color: 'red', name: 'health', subcategory: 'doctor', icon: 'add'},
        perMonth: 1,
        from: -1000,
        to: -10000
    },
    clothes: {
        category: {color: 'papayawhip', name: 'clothes', subcategory: '', icon: 'shirt'},
        perMonth: 2,
        from: -1000,
        to: -5000
    },
    salary: {
        category: {color: 'limegreen', name: 'salary', subcategory: '', icon: 'money'},
        perMonth: 1,
        from: 250000,
        to: 350000
    },
    money: {
        category: {color: 'limegreen', name: 'money', subcategory: '', icon: 'money'},
        perMonth: 0,
        from: 10000,
        to: 400000
    }
};

/**
 * Database seeding function
 *
 * USE ONLY FOR DEVELOPMENT!
 *
 *
 * @param user - userId to tell which user we want to seed with transactions
 * @param months - number of months to seed
 * @returns {transactions}
 */
let seedDB = function (user, months) {
    let transactions = [];

    for (let category in categories) {
        let actual = categories[category];
        let randomized = randomize(actual.category, months, actual.perMonth, actual.from, actual.to);
        transactions = transactions.concat(randomized);
    }

    transactions.map(transaction => {
        transaction.user = user._id;
        transaction.modifiedAt = new Date();
        transaction.createdAt = new Date();
    });
    transactions.sort((a, b) => a.date - b.date);
    return Transaction.create(transactions);
};

function randomize(category, months, perMonth, from, to) {
    let transactions = [];
    for (let i = 0; i < months; i++) {
        for (let j = 0; j < perMonth; j++) {
            let transaction = {
                date: randomDate(i),
                currency: 'HUF',
                category: category,
                amount: random(from, to)
            };
            transactions.push(transaction);
        }
    }
    return transactions;
}

function random(from, to) {
    return Math.round(Math.random() * (to - from)) + from;
}

function randomDate(months) {
    return moment().subtract(months, 'month').subtract(rndDays(), 'days').toDate();
}

function rndDays() {
    return Math.round(Math.random() * 30.5);
}

module.exports = seedDB;