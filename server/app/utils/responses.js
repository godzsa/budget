/**
 * Created by david on 2017.02.04..
 */

/**
 * Runs a function in async mode and returns status 200 if it went fine
 * returns error if promise is rejected
 *
 * @param req - request containing data needed for promise
 * @param res - respose
 * @param promise - function returning a promise
 */
const ok = (req, res, promise) => promise(req, res)
    .then(succ => res.status(200).json({success: true}))
    .catch(err => error(res, err));

/**
 * Runs a function in async mode and returns status 200 with data
 * if it is resolved, returns error if rejected
 *
 * @param req - request containing data needed for promise
 * @param res - respose
 * @param promise - function returning a promise
 */
const data = (req, res, promise) => promise(req, res)
    .then(data => res.status(200).json({success: true, data: data}))
    .catch(err => error(res, err));

/**
 * Returns status 400 and an error object
 *
 * @param res - respone
 * @param err - error object containing error message etc.
 */
const error = (res, err) => res.status(400).json({success: false, error: err});

module.exports = {ok, data, error};