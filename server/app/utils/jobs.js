'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
const schedule = require('node-schedule');
const RepeatingTransactionController = require('../controllers/repeating-transaction');

/**
 * Register your cron constants here
 */
const cron = {
    EVERYDAY: '0 1 0 * * *'
};

/**
 * Cron job runner
 * register cron jobs as variables here, they are invoked when the server is started
 */
const jobs = function () {
    let repeatingTransactionJob = schedule.scheduleJob(cron.EVERYDAY, function () {
        RepeatingTransactionController.run();
        console.log('Repeating Transactions added to DB!');
    });
};

module.exports = jobs;

