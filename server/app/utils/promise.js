'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */


/**
 * If condition is met returns Promise else rejects
 */
const resolveWhen = (condition, data, err) =>
    condition ? Promise.resolve(data) : Promise.reject({error: err});

/**
 * If condition is met returns with a Promise containing trueData
 * else we return falseData
 */
const resolveOr = (condition, trueData, falseData) =>
    condition ? Promise.resolve(trueData) : Promise.resolve(falseData);

module.exports = {resolveWhen, resolveOr};