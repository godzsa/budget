'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const moment = require('moment');

/**
 * Creates a request body mock
 *
 * @param name of the collection to mock
 * @param value which we want mock the request with
 * @param user request.user._id mock
 * @returns {{body: {}}}
 */
const requestBody = (name, value, user) => {
    let req = {body: {}};
    req.body[name] = value;
    if (user) {
        req.user = {};
        req.user._id = user;
    }
    return req;
};

/**
 * Creates a request params mock
 *
 * @param name of the param to mock
 * @param value which we want mock the request with
 * @param user request.user._id mock
 * @returns {{params: {}}}
 */
const requestParams = (name, value, user) => {
    let req = {params: {}};
    req.params[name] = value;
    if (user) {
        req.user = {};
        req.user._id = user;
    }
    return req;
};

/**
 * Seed the collection with dummy data
 *
 * @param model which we want to seed
 * @param amount of the items we want to add to the collection
 * @param data - base data to start the seeding with
 * @param transform - a function which transforms the base data so the items are different in the collection
 */
const seed = async (model, amount, data, transform) => {
    let ret = [];
    for (let i = 0; i < amount; i++) {
        let toSave = transform ? transform(data, i) : data;
        let savedData = await model.create(toSave);

        //just to make sure data is saved
        await setTimeout(() => Promise.resolve(''), 250);

        ret.push(savedData);
    }
    return ret;
};

/**
 * Transaction Model mock
 */
const transactionModel = () => ({
    category: {color: 'testColor', name: 'testCategName', subcategory: 'testSubCategory', icon: 'testIcon'},
    comment: 'testComment',
    amount: 1000,
    currency: 'testCurrency',
    date: moment('1992-10-10').toDate()
});

/**
 * User Model mock
 */
const userModel = (i) => {
    i = i ? i : "";
    return {
        name: 'testuser' + i,
        username: 'username' + i,
        password: '123',
        email: 'email' + i + '@email.com'
    }
};

module.exports = {requestBody, requestParams, seed, userModel, transactionModel};