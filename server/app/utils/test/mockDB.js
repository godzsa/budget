'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const config = require('config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(mongoose);


//TODO: sometimes tests timeout because of mockgoose downloading mongodb
// pass --timeout 120000 to mocha
before((done) => {

    console.log('DB_HOST', config.DBHost);
    mockgoose.prepareStorage()
        .then(() => mongoose.connect(config.DBHost))
        .then(() => {
            console.log('Mongoose mocked');
            done();
        }).catch(console.log);
});