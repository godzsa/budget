'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const jwt = require('jsonwebtoken');
const config = require('../../config/database');

/**
 * Creates a JWT token from data
 *
 * @param data
 * @returns {{token: (number|*)}}
 */
const createToken = data => {
    return {token: jwt.sign(data, config.secret)}
};

module.exports = {createToken};