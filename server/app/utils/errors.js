'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

/**
 * Create error message from mongo unique error
 * (mongoose does not provide one)
 *
 * @param error
 * @returns {{errors: {message: string}}}
 */
const uniqueError = function (error) {
    let re = /index:\s(.*)_/;
    let param = re.exec(error.errmsg)[1];
    let err = {errors: {message: `${param} must be unique`}};
    err[param] = {
        message: `${param} must be unique`,
        name: 'ValidatorError',
        properties: {type: "unique", path: param, message: `${param} must be unique`}
    };
    return err;
};


module.exports = {uniqueError};