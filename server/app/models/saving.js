'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SavingSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User', required: [true, 'User is required']},
    name: {type: String, required: [true, 'Saving name is required']},
    start: {type: Date, required: [true, 'Start Date is required']},
    end: {type: Date, required: [true, 'End Date is required']},
    categories: {
        type: [{
            category: {color: String, name: String, subcategory: String, icon: String},
            percent: Number,
        }], required: [true, 'Categories is required']
    }
});

module.exports = mongoose.model('Saving', SavingSchema);