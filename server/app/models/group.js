'use strict';

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.25..
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueError = require('../utils/errors').uniqueError;

//TODO: remove email field...
let GroupSchema = new Schema({
    users: {type: [{type: Schema.Types.ObjectId, ref: 'User'}], required: [true, 'User is required']},
    invitations: [{
        type: String, trim: true, lowercase: true,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please enter a valid email address']
    }]
});

GroupSchema.post('save', (error, doc, next) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        next(uniqueError(error));
    } else {
        next(error);
    }
});

module.exports = mongoose.model('Group', GroupSchema);