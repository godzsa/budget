'use strict';

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.25..
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const uniqueError = require('../utils/errors').uniqueError;

const SALT_FACTOR = 10;

let UserSchema = new Schema({
    name: {type: String, required: [true, 'Name is required'], trim: true},
    username: {type: String, required: [true, 'Username is required'], unique: true, trim: true},
    password: {type: String, required: [true, 'Password is required'], select: false},
    email: {
        type: String, unique: true, trim: true, lowercase: true, required: [true, 'Email is required'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please enter a valid email address']
    },
    categories: [{color: String, name: String, subcategory: String, icon: String}]
});

UserSchema.pre('save', function (next) {
    if (!this.isModified('password')) return next();
    bcrypt.hash(this.password, SALT_FACTOR)
        .then(hash => {
                this.password = hash;
                next();
            },
            err => next(err))
        .catch(err => next(err));
});

UserSchema.post('save', (error, doc, next) => {
    if (error.code === 11000) {
        next(uniqueError(error));
    } else {
        next(error);
    }
});

UserSchema.methods.valid = function (password) {
    //TODO: refactor to async
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);