'use strict';

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.25..
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TransactionSchema = require('./schemas/transaction-schema');

TransactionSchema.pre('save', function (next) {
    this.createdAt = new Date();
    next();
});

module.exports = mongoose.model('Transaction', TransactionSchema);