'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TransactionSchema = require('./schemas/transaction-schema');

let RepeatingTransactionSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    transaction: {type: TransactionSchema, required: [true, 'Transation is required']},
    period: {type: String, required: [true, 'Interval is required']},
    nextDate: {type: Date, required: [true, 'Date is required']}
});

module.exports = mongoose.model('RepeatingTransaction', RepeatingTransactionSchema);