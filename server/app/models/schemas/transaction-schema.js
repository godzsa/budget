'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');

let TransactionSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User', required: [true, 'User is required']},
    createdAt: {type: Date, default: new Date(), required: true},
    updatedAt: {type: Date, default: new Date(), required: true},
    category: {
        type: {
            color: {type: String, required: true},
            name: {type: String, required: true},
            subcategory: {type: String, required: true},
            icon: {type: String, required: true}
        },
        validate: {
            validator: function (v) {
                return _.every(['color', 'name', 'subcategory', 'icon'], _.partial(_.has, v))
            },
            message: '{VALUE} is not a valid category!'
        },
        required: [true, 'Category is required']
    },
    comment: {type: String, trim: true},
    amount: {type: Number, required: [true, 'Amount is required']},
    currency: {type: String, required: [true, 'Currency is required'], trim: true},
    date: {type: Date, required: [true, 'Date is required']}
});

module.exports = TransactionSchema;