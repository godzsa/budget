'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const express = require('express');
const routes = express.Router();
const CategoryController = require('../controllers/category');
const ok = require('../utils/responses').ok;
const data = require('../utils/responses').data;


module.exports = function () {
    routes.get('/', (req, res) => data(req, res, CategoryController.get));
    routes.post('/', (req, res) => ok(req, res, CategoryController.create));
    routes.delete('/:name', (req, res) => ok(req, res, CategoryController.remove));

    return routes;
};