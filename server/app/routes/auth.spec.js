// "use strict";
//
// /**
//  * Created by david on 2017.01.15..
//  */
//
//
// const mockDB = require('../utils/test/mockDB');
// const chai = require('chai');
// const chaiHttp = require('chai-http');
// const expect = chai.expect;
// const User = require('../models/user');
//
// chai.use(chaiHttp);
//
// describe('Auth testing', () => {
//
//     beforeEach(
//         () => dbUtils.clear(User)
//     );
//
//
//     describe('register', () => {
//         it('should save the user and return token if input is valid', done => {
//             let testUser = {name: 'testuser', username: 'username', password: '123', email: 'email@email.com'};
//
//             chai.request(server)
//                 .post('/auth/register')
//                 .send(testUser)
//                 .end((err, res) => {
//                     expect(err).to.be.null;
//                     expect(res).to.have.status(200);
//                     expect(res.body).to.be.a('object');
//                     expect(res.body.success).to.be.true;
//                     expect(res.body.data.token).to.be.a('string');
//
//                     done();
//                 });
//         });
//
//         it('should return error when user is already registered', done => {
//             let testUser = {name: 'testuser', username: 'username', password: '123', email: 'email@email.com'};
//             let user = new User({local: testUser});
//             user.save().then(success => {
//                 chai.request(server)
//                     .post('/auth/register')
//                     .send(testUser)
//                     .end((err, res) => {
//                         expect(err).not.be.null;
//                         expect(res).to.have.status(400);
//
//                         done();
//                     });
//             });
//         });
//
//         //TODO: test for email, name, username, password etc.
//         it('should return validation error when input is not valid', done => {
//             let testUser = {username: 'username', password: '123', email: 'email'};
//             chai.request(server)
//                 .post('/auth/register')
//                 .send(testUser)
//                 .end((err, res) => {
//                     expect(err).not.be.null;
//                     expect(res).to.have.status(400);
//
//                     done();
//                 });
//         });
//     });
//
//     describe('login', () => {
//         it('should return user token when user found', done => {
//             let testUser = {name: 'testuser', username: 'username', password: '123', email: 'email@email.com'};
//             let user = new User({local: testUser});
//             user.save().then(success => {
//                 chai.request(server)
//                     .post('/auth/login')
//                     .send(testUser)
//                     .end((err, res) => {
//                         expect(err).to.be.null;
//                         expect(res).to.have.status(200);
//                         expect(res.body).to.be.a('object');
//                         expect(res.body.success).to.be.true;
//                         expect(res.body.data.token).to.be.a('string');
//
//                         done();
//                     });
//             });
//         });
//
//         //TODO: test for username, password
//         it('should return error when user is not found', done => {
//             let testUser = {name: 'testuser', username: 'username', password: '123', email: 'email@email.com'};
//
//             let user = new User({local: testUser});
//             user.save().then(success => {
//                 chai.request(server)
//                     .post('/auth/login')
//                     .send({username: 'name', password: 'hola'})
//                     .end((err, res) => {
//                         expect(err).not.be.null;
//                         expect(res).to.have.status(400);
//
//                         done();
//                     });
//             });
//         });
//     });
//
//
// });