'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const express = require('express');
const StatisticsController = require('./../controllers/statistics');
const routes = express.Router();
const ok = require('../utils/responses').ok;
const data = require('../utils/responses').data;


module.exports = function () {
    routes.post('/pie', (req, res) => data(req, res, StatisticsController.pie));
    routes.post('/group/pie', (req, res) => data(req, res, StatisticsController.groupPie));

    routes.post('/line', (req, res) => data(req, res, StatisticsController.line));
    routes.post('/group/line', (req, res) => data(req, res, StatisticsController.groupLine));

    return routes;
};