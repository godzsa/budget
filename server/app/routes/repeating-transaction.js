'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const express = require('express');
const RepeatingTransactionController = require('./../controllers/repeating-transaction');
const routes = express.Router();
const ok = require('../utils/responses').ok;
const data = require('../utils/responses').data;

module.exports = function () {
    routes.post('/get', (req, res) => data(req, res, RepeatingTransactionController.get));
    routes.post('/', (req, res) => data(req, res, RepeatingTransactionController.create));
    routes.put('/', (req, res) => ok(req, res, RepeatingTransactionController.edit));
    routes.delete('/:id', (req, res) => ok(req, res, RepeatingTransactionController.remove));

    return routes;
};