'use strict';

/**
 * Created by david on 2017.02.03..
 */

const express = require('express');
const GroupController = require('./../controllers/group');
const routes = express.Router();
const ok = require('../utils/responses').ok;
const data = require('../utils/responses').data;

module.exports = function () {
    routes.get('/', (req, res) => data(req, res, GroupController.get));
    routes.post('/', (req, res) => ok(req, res, GroupController.create));
    routes.post('/invitation', (req, res) => ok(req, res, GroupController.invite));
    routes.delete('/invitation/:email', (req, res) => ok(req, res, GroupController.revokeInvitation));
    routes.post('/join', (req, res) => ok(req, res, GroupController.join));
    routes.delete('/join', (req, res) => ok(req, res, GroupController.leave));

    return routes;
};