'use strict';

/**
 * Created by david on 2017.02.03..
 */

const express = require('express');
const TransactionController = require('./../controllers/transaction').TransactionController;
const routes = express.Router();
const ok = require('../utils/responses').ok;
const data = require('../utils/responses').data;

module.exports = function () {
    routes.post('/batch', (req, res) => data(req, res, TransactionController.batch));
    routes.post('/', (req, res) => data(req, res, TransactionController.create));
    routes.put('/', (req, res) => ok(req, res, TransactionController.edit));

    routes.delete('/:id', (req, res) => ok(req, res, TransactionController.remove));
    routes.post('/group/batch', (req, res) => data(req, res, TransactionController.groupBatch));

    routes.get('/seed/:months', (req, res) => ok(req, res, TransactionController.seed));

    return routes;
};