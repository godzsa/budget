'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */


const express = require('express');
const routes = express.Router();
const SavingController = require('../controllers/saving');
const ok = require('../utils/responses').ok;
const data = require('../utils/responses').data;

module.exports = function () {
    routes.get('/', (req, res) => data(req, res, SavingController.get));
    routes.post('/', (req, res) => ok(req, res, SavingController.create));
    routes.delete('/:id', (req, res) => ok(req, res, SavingController.remove));
    routes.get('/new', (req, res) => data(req, res, SavingController.newSavingInfo));

    return routes;
};