'use strict';

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.26..
 */

const express = require('express');
const routes = express.Router();
const UserController = require('../controllers/user');
const data = require('../utils/responses').data;

module.exports = function () {
    routes.post('/login', (req, res) => data(req, res, UserController.login));
    routes.post('/register', (req, res) => data(req, res, UserController.register));

    return routes;
};