'use strict';

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.25.
 *
 * This is the start file for the budget server.
 * Dev - run with 'nodemon'
 * Prod - run with 'node'
 */

const express = require('express');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('config');
const ok = require('./utils/responses').ok;
const app = express();
const port = process.env.PORT || 8080;
const jobs = require('./utils/jobs')();

console.log('Using DB:', config.DBHost);
mongoose.connect(config.DBHost);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error:'));

//middleware
app.use(bodyParser.json());
app.use(cors());
app.use(passport.initialize());
require('./passport')(passport);

//routes
const auth = require('./routes/auth')();
const transactions = require('./routes/transactions')();
const repeating = require('./routes/repeating-transaction')();
const groups = require('./routes/groups')();
const categories = require('./routes/categories')();
const savings = require('./routes/savings')();
const statistics = require('./routes/statistics')();

//unauthorized routes
app.use('/auth', auth);

//authorized routes
app.all('*', passport.authenticate('jwt', {session: false}));

app.get('/ping', (req, res) => ok(req, res, () => Promise.resolve(null)));
app.use('/transactions', transactions);
app.use('/groups', groups);
app.use('/category', categories);
app.use('/savings', savings);
app.use('/statistics', statistics);
app.use('/repeating', repeating);

module.exports = app.listen(port, () => console.log('Listening on port: ' + port));
