'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const chaiSubset = require('chai-subset');

const config = require('config');
const mock = require('../utils/test/mock');
const dbUtils = require('../utils/test/mockDB');
const User = require('../models/user');
const Group = require('../models/group');
const Transaction = require('../models/transaction');
const TransactionController = require('../controllers/transaction').TransactionController;
const moment = require('moment');

//Chai middlewares
chai.should();
chai.use(chaiAsPromised);
chai.use(chaiSubset);
chai.config.truncateThreshold = 0;

describe('Transaction Controller and Model tests', function () {
    let user;
    let testTransaction;

    beforeEach(async function () {
        user = new User(mock.userModel());
        await user.save();

        testTransaction = mock.transactionModel();
        return Transaction.remove({});
    });

    afterEach(async function () {
        await User.remove({});
    });

    describe('Creating transaction', function () {
        it('should return error if category is invalid', function () {
            testTransaction.category = 'invalid';
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected;
        });

        it('should return error if amount is not a number', function () {
            testTransaction.amount = 'invalid';
            TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected;
            testTransaction.amount = new Date();
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected;
        });

        it('should return error if date is not a Date', function () {
            testTransaction.date = 'invalid';
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected;
        });


        it('should not save transaction if category is not set', function () {
            delete testTransaction.category;
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected
                .and.eventually.have.deep.property('errors.category')
                .and.have.property('message', 'Category is required');
        });

        it('should not save transaction if date is not set', function () {
            delete testTransaction.date;
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected
                .and.eventually.have.deep.property('errors.date')
                .and.have.property('message', 'Date is required');
        });

        it('should not save transaction if amount is not set', function () {
            delete testTransaction.amount;
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected
                .and.eventually.have.deep.property('errors.amount')
                .and.have.property('message', 'Amount is required');
        });

        it('should not save transaction if currency is not set', function () {
            delete testTransaction.currency;
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id))
                .should.be.rejected
                .and.eventually.have.deep.property('errors.currency')
                .and.have.property('message', 'Currency is required');
        });

        it('should save transaction if comment is not set', async function () {
            delete testTransaction.comment;
            let created = await TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
            created.should.containSubset(testTransaction).and.have.property('_id');
            created.should.have.property('createdAt');
            created.should.have.property('updatedAt');
            created.should.have.property('user');
        });


        it('should save transaction with provided data', async function () {
            let created = await TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
            created.should.containSubset(testTransaction).and.have.property('_id');
            created.should.have.property('createdAt');
            created.should.have.property('updatedAt');
            created.should.have.property('user');
        });

        it('should save transaction and rewrite _id if set', async function () {
            testTransaction._id = "dummy id";
            let created = await TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
            created.should.containSubset(testTransaction).and.have.property('_id');
            created.should.have.property('createdAt');
            created.should.have.property('updatedAt');
            created.should.have.property('user');
        });
    });

    describe('Querying transactions', function () {
        it('should return first batch of transactions if batch is invalid', async function () {
            testTransaction.user = user._id;
            let transactions = await mock.seed(Transaction, 19, testTransaction);
            let savedTransactions = await TransactionController.batch(mock.requestBody('batch', 'invalidBatchType', user._id))
                .should.eventually.have.lengthOf(config.batchSize);
            savedTransactions.map(transaction => transaction.should.containSubset(testTransaction));
        });

        it('should return first batch of transactions for user set in body', async function () {
            testTransaction.user = user._id;
            let transactions = await mock.seed(Transaction, 19, testTransaction);
            let savedTransactions = await TransactionController.batch(mock.requestBody('user', user._id))
                .should.eventually.have.lengthOf(config.batchSize);
            savedTransactions.map(transaction => transaction.should.containSubset(testTransaction));
        });

        it('should throw error if user set in body is invalid', async function () {
            testTransaction.user = user._id;
            let transactions = await mock.seed(Transaction, 19, testTransaction);
            let savedTransactions = await TransactionController.batch(mock.requestBody('user', 'invalid'))
                .should.be.rejected;
        });

        it('should return first batch of transactions if batch is undefined', async function () {
            testTransaction.user = user._id;
            let transactions = await mock.seed(Transaction, 19, testTransaction);
            let savedTransactions = await TransactionController.batch(mock.requestBody('batch', undefined, user._id))
                .should.eventually.have.lengthOf(config.batchSize);
            savedTransactions.map(transaction => transaction.should.containSubset(testTransaction));
        });

        it('should return empty array if there are no transactions for the user', function () {
            return TransactionController.batch(mock.requestBody('batch', 0, user._id))
                .should.eventually.be.empty;
        });

        it('should return empty array if there are no transactions for the user but other user does', async function () {
            //make sure other user has data
            testTransaction.user = user._id;
            await mock.seed(Transaction, 10, testTransaction);
            //create new user
            let newUser = await User.create(mock.userModel(1));
            //new user has no transactions
            return TransactionController.batch(mock.requestBody('batch', 0, newUser._id))
                .should.eventually.be.empty;
        });

        it('should return array containing one transaction', async function () {
            let created = await TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
            let savedTransactions = await TransactionController.batch(mock.requestBody('batch', 0, user._id))
                .should.eventually.have.lengthOf(1);
            savedTransactions[0].should.containSubset(testTransaction);
        });

        it('should return array containing transactions if batch is bigger then the amount of transactions', async function () {
            let length = 3;
            testTransaction.user = user._id;
            let transactions = await mock.seed(Transaction, length, testTransaction);
            let savedTransactions = await TransactionController.batch(mock.requestBody('batch', 0, user._id))
                .should.eventually.have.lengthOf(length);
            savedTransactions.map(transaction => transaction.should.containSubset(testTransaction));
        });


        it('should return array containing transactions in batches', async function () {
            //setup
            let length = 7;
            testTransaction.user = user._id;
            let transform = (transaction, i) => {
                transaction.comment = 'test' + i;
                return transaction;
            };
            let transactions = await mock.seed(Transaction, length, testTransaction, transform);
            //transactions are queried in reverse order
            transactions = transactions.reverse();

            //test
            //first batch
            let savedTransactions = await TransactionController.batch(mock.requestBody('batch', 0, user._id));
            savedTransactions.should.have.lengthOf(5);
            savedTransactions.map((transaction, i) => transaction.toObject().should.containSubset(transactions[i].toObject()));
            //second batch
            savedTransactions = await TransactionController.batch(mock.requestBody('batch', 1, user._id));
            savedTransactions.should.have.length.lengthOf(2);
            savedTransactions.map((transaction, i) => transaction.toObject().should.containSubset(transactions[i + config.batchSize].toObject()));
        });
    });


    describe('Editing transactions', function () {

        beforeEach(() => {
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
        });

        afterEach(async () => {
            return await Transaction.remove();
        });

        it('should return null and not modify anything if transactionId is not existing in db', async function () {
            //create transaction
            let newTransaction = await TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
            //delete transaction
            await Transaction.findByIdAndRemove(newTransaction._id);
            //should not find the transaction which we want to edit
            TransactionController.edit(mock.requestBody('transaction', {_id: newTransaction._id}, user._id))
                .should.eventually.be.null;
            //should contain the original element as was
            let original = (await Transaction.find())[0]
                .should.containSubset(testTransaction);
        });

        it('should reject promise if transaction is not set', function () {
            let category = {color: 'testColor2', icon: 'testIcon2', name: 'testName2', subcategory: 'testSubCategory2'};
            return TransactionController.edit(mock.requestBody('transaction', null, user._id))
                .should.be.rejected
                .and.eventually.have.deep.property('message', 'Transaction ID is not set!');
        });

        it('should update only the category', async function () {
            //query the original and change it
            let original = (await Transaction.find())[0];

            let category = {color: 'testColor2', icon: 'testIcon2', name: 'testName2', subcategory: 'testSubCategory2'};
            original.category = category;
            //we set the updatedAt manually (on client side)
            original.updatedAt = new Date();

            //we run the update query
            let changed = await TransactionController.edit(mock.requestBody('transaction', original, user._id));
            changed = changed.toObject();

            //it should only change the category field
            delete testTransaction.category;
            changed.should.containSubset(testTransaction);
            changed.category.should.eql(category);
        });

        it('should update only the date', async function () {
            //query the original and change it
            let original = (await Transaction.find())[0];

            let date = new Date('2015-04-11');
            original.date = date;
            //we set the updatedAt manually (on client side)
            original.updatedAt = new Date();

            //we run the update query
            let changed = await TransactionController.edit(mock.requestBody('transaction', original, user._id));
            changed = changed.toObject();

            //it should only change the category field
            delete testTransaction.date;
            changed.should.containSubset(testTransaction);
            changed.date.should.eql(date);
        });

        it('should update the amount only', async function () {
            //query the original and change it
            let original = (await Transaction.find())[0];

            let amount = 1234;
            original.amount = amount;
            //we set the updatedAt manually (on client side)
            original.updatedAt = new Date();

            //we run the update query
            let changed = await TransactionController.edit(mock.requestBody('transaction', original, user._id));
            changed = changed.toObject();

            //it should only change the category field
            delete testTransaction.amount;
            changed.should.containSubset(testTransaction);
            changed.amount.should.eql(amount);
        });

        it('should update the currency only', async function () {
            //query the original and change it
            let original = (await Transaction.find())[0];

            let currency = "testCurrency2";
            original.currency = currency;
            //we set the updatedAt manually (on client side)
            original.updatedAt = new Date();

            //we run the update query
            let changed = await TransactionController.edit(mock.requestBody('transaction', original, user._id));
            changed = changed.toObject();

            //it should only change the category field
            delete testTransaction.currency;
            changed.should.containSubset(testTransaction);
            changed.currency.should.eql(currency);
        });

        it('should update the comment only', async function () {
            //query the original and change it
            let original = (await Transaction.find())[0];

            let comment = "testComment2";
            original.comment = comment;
            //we set the updatedAt manually (on client side)
            original.updatedAt = new Date();

            //we run the update query
            let changed = await TransactionController.edit(mock.requestBody('transaction', original, user._id));
            changed = changed.toObject();

            //it should only change the category field
            delete testTransaction.comment;
            changed.should.containSubset(testTransaction);
            changed.comment.should.eql(comment);
        });

        it('should update every property set', async function () {
            //query the original and change it
            let original = (await Transaction.find())[0];

            let amount = 1234;
            original.amount = amount;
            let comment = 'testComment2';
            original.comment = comment;
            //we set the updatedAt manually (on client side)
            original.updatedAt = new Date();

            //we run the update query
            let changed = await TransactionController.edit(mock.requestBody('transaction', original, user._id));
            changed = changed.toObject();

            //it should only change the category field
            delete testTransaction.amount;
            delete testTransaction.comment;
            changed.should.containSubset(testTransaction);
            changed.comment.should.eql(comment);
            changed.amount.should.eql(amount);
        });

        it('should update every property', async function () {
            //query the original and change it
            let original = (await Transaction.find())[0];

            let changed = {
                _id: original._id,
                amount: 1234,
                comment: 'testComment2',
                category: {icon: 'testIcon2', color: 'testColor2', name: 'testCategory2', subcategory: 'subcategory2'},
                date: new Date(),
                currency: 'testCurrency2'
            };
            //we set the updatedAt manually (on client side)
            changed.updatedAt = new Date();

            //we run the update query
            let changedSaved = await TransactionController.edit(mock.requestBody('transaction', changed, user._id));
            changedSaved = changedSaved.toObject();

            //it should only change the category field
            changedSaved.should.not.containSubset(testTransaction);
            changedSaved.should.containSubset(changed);
        });
    });

    describe('Removing transactions', function () {

        beforeEach(() => {
            return TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
        });

        afterEach(async () => {
            return await Transaction.remove();
        });

        it('should reject promise if ID is undefined or null', function () {
            return TransactionController.remove({params: {id: undefined}})
                .should.be.rejected
                .and.eventually.have.deep.property('message', 'Transaction ID is not set!');
        });

        it('should return null and not remove anything', async function () {
            //create transaction
            let newTransaction = await TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
            //delete transaction
            await Transaction.findByIdAndRemove(newTransaction._id);
            //should not find the transaction which we want to delete
            await TransactionController.remove(mock.requestParams('id', newTransaction._id, user._id))
                .should.eventually.be.null;
            //should contain the original element as was
            let original = await Transaction.find();
            original.should.have.lengthOf(1);
            original[0].should.containSubset(testTransaction);

        });

        it('should delete the transaction but not others', async function () {
            //create transaction
            let newTransaction = await TransactionController.create(mock.requestBody('transaction', testTransaction, user._id));
            //delete transaction
            await TransactionController.remove(mock.requestParams('id', newTransaction._id, user._id))
                .should.eventually.be.resolved;

            //should contain the original element as was
            let original = await Transaction.find().should.eventually.have.lengthOf(1);
            original[0].should.containSubset(testTransaction);
        });
    });

    describe('Querying group transactions', function () {
        let user1Member;
        let user2Member;
        let user3Invited;
        let group;

        beforeEach(async () => {
            user1Member = await User.create(mock.userModel(1));
            user2Member = await User.create(mock.userModel(2));
            user3Invited = await User.create(mock.userModel(3));
            group = await Group.create({users: [user1Member._id, user2Member._id], invitations: [user3Invited.email]});
        });

        it('should return first batch if batch is not set', async function () {
            //setup

            //transform data so every transaction is unique
            let transform = (transaction, i) => {
                transaction.date = moment().subtract(i + 1, 'day').toDate();
                return transaction;
            };
            //create user1Member transactions who shares a group with user2Member
            testTransaction.user = user1Member;
            let user1Transactions = await mock.seed(Transaction, 6, testTransaction, transform);
            //create one transaction for user2Member
            testTransaction.date = new Date();
            testTransaction.user = user2Member;
            let user2Transactions = await Transaction.create(testTransaction);
            //create request body
            let req = mock.requestBody('batch', 'invalidBatchType', user1Member._id);

            //test
            let savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(config.batchSize);
            savedTransactions.map((transaction, i) => {
                transaction = transaction.toObject();
                if (i === 0) {
                    //transactions should not return user password
                    let user2Transaction = user2Transactions.toObject();
                    let user = user2Transaction.user;
                    delete user.password;
                    user2Transaction.user = user;
                    transaction.should.containSubset(user2Transaction);
                } else {
                    //transactions should not return user password
                    let user1Transaction = user1Transactions[i - 1].toObject();
                    let user = user1Transaction.user;
                    delete user.password;
                    user1Transaction.user = user;
                    transaction.should.containSubset(user1Transaction);
                }
            });
        });


        it('should return empty array if user has no group and no transactions', function () {
            return TransactionController.groupBatch(mock.requestBody('batch', 0, user1Member._id))
                .should.eventually.be.empty;
        });

        it('should return empty array if user has only invitation to group and no transactions', async function () {
            //make sure one group member has transactions
            testTransaction.user = user1Member._id;
            await mock.seed(Transaction, 6, testTransaction);

            return TransactionController.groupBatch(mock.requestBody('batch', 0, user3Invited._id))
                .should.eventually.be.rejected;
        });

        it('should return empty array if user has group but no one in group has transactions', function () {
            return TransactionController.groupBatch(mock.requestBody('batch', 0, user1Member._id))
                .should.eventually.be.empty;
        });

        it('should return empty array if users in group does not have transactions but invited users have', async function () {
            //make sure one group member has transactions
            testTransaction.user = user3Invited._id;
            await mock.seed(Transaction, 6, testTransaction);

            return TransactionController.groupBatch(mock.requestBody('batch', 0, user1Member._id))
                .should.eventually.be.empty;
        });

        it('should return transaction if user has group and at least one transaction', async function () {
            //setup
            let length = 3;
            let user = user1Member.toObject();
            delete user.password;
            testTransaction.user = user;
            //mock
            await mock.seed(Transaction, length, testTransaction);
            let req = mock.requestBody('batch', 0, user1Member._id);
            //test
            let savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(length);
            savedTransactions.map(transaction => transaction.toObject().should.containSubset(testTransaction));
        });

        it('should return transaction in batches if user has group and more transactions than batch limit', async function () {
            //setup
            let length = 7;
            let user = user1Member.toObject();
            delete user.password;
            testTransaction.user = user;
            let seeded = await mock.seed(Transaction, length, testTransaction);
            //we need to 'populate' users and reverse the order because that is what batch query does
            seeded = seeded.reverse();
            seeded = seeded.map(transaction => {
                transaction = transaction.toObject();
                transaction.user = user;
                return transaction;
            });

            //test first batch
            let req = mock.requestBody('batch', 0, user1Member._id);
            let savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(config.batchSize);
            savedTransactions.map((transaction, i) => transaction.toObject().should.containSubset(seeded[i]));


            //test second batch
            req = mock.requestBody('batch', 1, user1Member._id);
            savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(length - config.batchSize);
            savedTransactions.map((transaction, i) => transaction.toObject()
                .should.containSubset(seeded[i + config.batchSize])
            );
        });

        it('should return transactions in batches if user does not have any transactions, but someone in the group has', async function () {
            //setup
            let length = 7;
            let user = user1Member.toObject();
            delete user.password;
            testTransaction.user = user;
            let seeded = await mock.seed(Transaction, length, testTransaction);
            //we need to 'populate' users and reverse the order because that is what batch query does
            seeded = seeded.reverse();
            seeded = seeded.map(transaction => {
                transaction = transaction.toObject();
                transaction.user = user;
                return transaction;
            });

            //test first batch with other user
            let req = mock.requestBody('batch', 0, user2Member._id);
            let savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(config.batchSize);
            savedTransactions.map((transaction, i) => transaction.toObject().should.containSubset(seeded[i]));


            //test second batch with other user
            req = mock.requestBody('batch', 1, user2Member._id);
            savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(length - config.batchSize);
            savedTransactions.map((transaction, i) => transaction.toObject()
                .should.containSubset(seeded[i + config.batchSize])
            );
        });

        it('should return every users transactions in batches in the group', async function () {
            //setup
            let length = 9;

            let user1 = user1Member.toObject();
            let user2 = user1Member.toObject();
            //requests does not return password
            delete user1.password;
            delete user2.password;
            //every second transaction belongs to the same user
            let transform = (transaction, i) => {
                transaction.user = i % 2 === 0 ? user2 : user1;
                transaction.date = moment().add(i, 'day');
                return transaction;
            };

            let seeded = await mock.seed(Transaction, length, testTransaction, transform);
            //we need to 'populate' users and reverse the order because that is what batch query does
            seeded = seeded.reverse();
            seeded = seeded.map(transaction => {
                transaction = transaction.toObject();
                transaction.user = this.user === user1._id ? user1 : user2;
                return transaction;
            });

            //test first batch with other user
            let req = mock.requestBody('batch', 0, user2Member._id);
            let savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(config.batchSize);
            savedTransactions.map((transaction, i) => transaction.toObject().should.containSubset(seeded[i]));


            //test second batch with other user
            req = mock.requestBody('batch', 1, user2Member._id);
            savedTransactions = await TransactionController.groupBatch(req)
                .should.eventually.have.lengthOf(length - config.batchSize);
            savedTransactions.map((transaction, i) => transaction.toObject()
                .should.containSubset(seeded[i + config.batchSize])
            );
        });
    });
});