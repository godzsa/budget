'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const mongoose = require('mongoose');
const Transaction = require('../models/transaction');
const Group = require('../models/group');

let StatisticsController = {

    pie: req => {
        return Transaction.aggregate([
            {$match: {user: getUserFromReq(req)}},
            {$group: groupByDateAndCategory(req)},
            {$group: groupByCategory},
            {$sort: {"_id.year": -1, "_id.interval": -1}},
            //transformToPie
        ])
    },

    groupPie: req => {
        return Group.findOne({users: req.user._id})
            .then(group => Transaction.aggregate([
                {$match: {user: {$in: group.users}}},
                {$group: groupByDateAndCategory(req)},
                {$group: groupByCategory},
                {$sort: {"_id.year": -1, "_id.interval": -1}},
            ]))
    },

    line: req => {
        return Transaction.aggregate([
            {$match: {user: getUserFromReq(req)}},
            {$group: groupByDay()},
            {$sort: {"_id.day": 1}},
            {$group: groupByDate(req)},
            {$sort: {"_id.year": 1, "_id.interval": 1}},
            {$project: {_id: 0,}}
        ])
    },

    groupLine: req => {
        return Group.findOne({users: req.user._id})
            .then(group => Transaction.aggregate([
                {$match: {user: {$in: group.users}}},
                {$group: groupByDay()},
                {$sort: {"_id.day": 1}},
                {$group: groupByDate(req)},
                {$sort: {"_id.year": 1, "_id.interval": 1}},
                {$project: {_id: 0,}}
            ]))
    }

};
//TODO: code review needed under this line ========

let groupByDay = () => {
    return {
        _id: {day: {$dayOfYear: "$date"}, year: {$year: "$date"}},
        amount: {$sum: "$amount"},
        date: {$first: "$date"},
    }
};

let groupByDate = (req) => {
    let period = {};
    period["$" + req.body.period] = "$date";
    return {
        _id: {period: req.body.period, interval: period, year: {$year: "$date"}},
        transactions: {$push: {amount: "$amount", date: "$date"}}
    }
};

let groupByDateAndCategory = (req) => {
    let period = {};
    period["$" + req.body.period] = "$date";
    return {
        _id: {period: req.body.period, interval: period, year: {$year: "$date"}, category: "$category"},
        transaction: {$push: {category: "$category", amount: "$amount"}}
    }
};

let groupByCategory = {
    _id: {interval: "$_id.interval", period: "$_id.period", year: "$_id.year"},
    categories: {
        $push: {
            category: {$arrayElemAt: ["$transaction.category", 0]},
            sum: {$sum: "$transaction.amount"},
        },
    }
};

function getUserFromReq(req) {
    if (req.body.user) {
        return mongoose.Types.ObjectId(req.body.user);
    }
    return req.user._id;
}


module.exports = StatisticsController;