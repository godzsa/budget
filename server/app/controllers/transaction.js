'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const config = require('config');
const Transaction = require('../models/transaction');
const Group = require('../models/group');
const seed = require('../seed');
const _ = require('lodash');

const TransactionController = {

    batch: req => {
        return Transaction.find({user: req.body.user || req.user._id})
            .skip(req.body.batch * config.batchSize)
            .limit(config.batchSize)
            .sort({date: -1, createdAt: -1})
    },

    create: req => {
        return createTransactionFromData(req.body.transaction, req.user._id);
    },

    remove: req => {
        if (_.isEmpty(req.params.id)) {
            return Promise.reject({message: 'Transaction ID is not set!'});
        }
        return Transaction.findByIdAndRemove(req.params.id);
    },

    edit: req => {
        if (_.isEmpty(req.body.transaction) || _.isEmpty(req.body.transaction._id)) {
            return Promise.reject({message: 'Transaction ID is not set!'});
        }
        return Transaction.findOneAndUpdate(
            {_id: req.body.transaction._id, updatedAt: {$lt: req.body.transaction.updatedAt}},
            req.body.transaction, {new: true})
    },

    groupBatch: req => {
        return Group.findOne({users: req.user._id})
            .then(group => Transaction.find({user: {$in: group.users}})
                .skip(req.body.batch * config.batchSize)
                .limit(config.batchSize)
                .sort({date: -1, createdAt: -1})
                .populate('user')
            );
    },

    seed: req => seed(req.user, req.params.months),
};

const createTransactionFromData = (transaction, user) => {
    transaction.user = user;
    delete transaction._id;

    let newTransaction = new Transaction(transaction);
    return newTransaction.save();
};

module.exports = {
    createTransactionFromData,
    TransactionController
};