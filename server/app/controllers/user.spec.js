"use strict";

/**
 * Created by david on 2017.01.15..
 */

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chaiAsPromised = require('chai-as-promised');

const mock = require('../utils/test/mock');
const dbUtils = require('../utils/test/mockDB');
const User = require('../models/user');
const UserController = require('../controllers/user');
const jwt = require('../utils/jwt');

chai.should();
chai.use(sinonChai);
chai.use(chaiAsPromised);

/**
 * Unit test for auth routes
 */
let testUser;
describe('User Controller and Model tests', function () {

    beforeEach(function () {
        testUser = mock.userModel();
        return User.remove({});
    });

    describe('Register', function () {
        it('should throw error if name is not set', function () {
            delete testUser.name;
            return UserController.register(mock.requestBody('user', testUser))
                .should.be.rejected
                .and.eventually.have.deep.property('errors.name.message', 'Name is required');
        });

        it('should throw error if username is not set', function () {
            delete testUser.username;
            return UserController.register(mock.requestBody('user', testUser))
                .should.be.rejected
                .and.eventually.have.a.deep.property('errors.username.message', 'Username is required');
        });

        it('should throw error if password is not set', function () {
            delete testUser.password;
            return UserController.register(mock.requestBody('user', testUser))
                .should.be.rejected
                .and.eventually.have.a.deep.property('errors.password.message', 'Password is required');
        });

        it('should throw error if email is not set', function () {
            delete testUser.email;
            return UserController.register(mock.requestBody('user', testUser))
                .should.be.rejected
                .and.eventually.have.a.deep.property('errors.email.message', 'Email is required');
        });


        it('should save the user and return token if input is valid', async function () {
            let createTokenStub = sinon.stub(jwt, 'createToken');
            createTokenStub.onCall(0).returns('token');

            await UserController.register(mock.requestBody('user', testUser)).should.eventually.equal('token');
            createTokenStub.restore();

            return User.find({username: testUser.username}).should.resolve;
        });

        xit('should return error when user is already registered', async function () {
            //TODO: we are skipping this test for now because error message is different when mocked, and dont want to mess with prodoction code

            await UserController.register(mock.requestBody('user', testUser));
            return UserController.register(mock.requestBody('user', testUser)).should.be.rejected
                .and.eventually.have.deep.property('errors.message');
        });

        xit('should return error when email is already in use', async function () {
            //we are skipping this test for now because error message is different when mocked, and dont want to mess with prodoction code

            await UserController.register(mock.requestBody('user', testUser));
            testUser.username = 'testUser2';

            return UserController.register(mock.requestBody('user', testUser)).should.be.rejected
                .and.eventually.have.deep.property('errors.message');
        });

        it('should return error when email is not valid', async function () {
            testUser.email = 'invalid';
            let registered = await UserController.register(mock.requestBody('user', testUser)).should.be.rejected;
            registered.should.have.deep.property('errors.email.message', 'Please enter a valid email address');
        });
    });

    describe('Login', function () {
        it('should return error when user is not found', async function () {
            await UserController.register(mock.requestBody('user', testUser));
            return UserController.login(mock.requestBody('user', {
                username: 'foo',
                password: 'bar'
            })).should.be.rejected;
        });

        it('should return error when password is not correct', async function () {
            await UserController.register(mock.requestBody('user', testUser));
            return UserController.login(mock.requestBody('user', {
                username: 'username',
                password: 'wrongpass'
            })).should.be.rejected.and.eventually.have.deep.property('error.message', 'Incorrect password');
        });

        it('should return user token when user found', async function () {
            await UserController.register(mock.requestBody('user', testUser));
            let createTokenStub = sinon.stub(jwt, 'createToken');
            createTokenStub.onCall(0).returns('token');

            await UserController.login(mock.requestBody('user', testUser)).should.eventually.equal('token');
            createTokenStub.restore();
        });
    });
})
;
