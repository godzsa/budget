'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const jwt = require('jsonwebtoken');
const config = require('../../config/database');
const User = require('../models/user');
const JWT = require('../utils/jwt');
const resolveWhen = require('../utils/promise').resolveWhen;

const UserController = {

    login: req => {
        return User.findOne({username: req.body.user.username})
            .select('+password')
            .then(user => resolveWhen(
                user.valid(req.body.user.password),
                JWT.createToken({id: user._id}),
                {message: 'Incorrect password'})
            )
    },

    register: req => {
        let newUser = new User(req.body.user);
        return newUser.save().then(user => Promise.resolve(JWT.createToken({id: user._id})))
    }
};

module.exports = UserController;