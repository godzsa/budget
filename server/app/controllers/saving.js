'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */


const Saving = require('../models/saving');
const Transaction = require('../models/transaction');
const moment = require('moment');
const _ = require('lodash');

const SavingController = {
    get: async req => { //TODO: refactor
        let savings = await getSavings(req.user).exec();
        let savingsAvgSpent = await Promise.all(
            savings.map(async actualSaving => {
                let saving = actualSaving.toObject();
                let avgs = await getAvgByCategoriesByDate(req.user, saving.start).exec();
                let spent = await getSpentBetweenByCategories(req.user, saving.start, saving.end).exec();
                saving.categories = mergeByCategories(saving, avgs, spent);
                return saving;
            })
        );
        return Promise.resolve(savingsAvgSpent);
    },

    create: req => {
        let raw = req.body.saving;
        raw.user = req.user._id;
        delete raw._id;
        let saving = new Saving(raw);

        return saving.save();
    },

    remove: req => Saving.findByIdAndRemove(req.params.id),

    newSavingInfo: req => getAvgByCategoriesByDate(req.user, new Date())
};

const getSavings = (user) => Saving.find({user: user._id});

const getAvgByCategoriesByDate = (user, date) => {
    return Transaction.aggregate(
        {$match: {user: user._id, amount: {$lt: 0}, date: {$lt: date}}},
        {$group: sumCategoriesByMonth},
        {$group: avgExpenseByCategory},
        {$project: {category: "$_id", _id: 0, avg: 1}}
    )
};

const getSpentBetweenByCategories = (user, start, end) => {
    return Transaction.aggregate(
        {$match: {user: user._id, amount: {$lt: 0}, date: {$gte: start, $lt: end}}},
        {$group: {_id: "$category", spent: {$sum: "$amount"}}},
        {$project: {category: "$_id", _id: 0, spent: 1}}
    )
};

const sumCategoriesByMonth = {
    _id: {year: {$year: "$date"}, month: {$month: "$date"}, category: "$category"},
    sum: {$sum: "$amount"}
};
const avgExpenseByCategory = {
    _id: "$_id.category",
    avg: {$avg: "$sum"}
};

const mergeByCategories = (saving, averages, spendings) => {
    let categories = [...saving.categories, ...averages, ...spendings];
    let groups = _.groupBy(categories, category => categoryToString(category.category));
    return Object.values(groups).map(group => _.merge({percent: 100, avg: 0, spent: 0}, ...group));
};

const categoryToString = (category) => {
    return category.name + "_" + category.icon + "_" + category.color + "_" + category.subcategory;
};

module.exports = SavingController;
