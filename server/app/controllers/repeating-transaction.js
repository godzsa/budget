'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const RepeatingTransaction = require('../models/repeating-transaction');
const createTransactionFromData = require('../controllers/transaction').createTransactionFromData;
const seed = require('../seed');
const moment = require('moment');

const RepeatingTransactionController = {
    get: req => {
        return RepeatingTransaction.find({user: req.user._id}).sort({nextDate: -1});
    },

    create: async req => {
        let repeating = req.body.transaction;
        let transaction = await createTransactionFromData(repeating.transaction, req.user._id);
        return createRepeatingTransactionFromData(repeating, transaction);
    },

    remove: req => {
        return RepeatingTransaction.findByIdAndRemove(req.params.id)
    },

    edit: req => {
        return RepeatingTransaction.findByIdAndUpdate(req.body.transaction._id, req.body.transaction)
    },

    run: async () => {
        let today = moment().startOf('day').toDate();
        let repeatingTransactions = await RepeatingTransaction.find({nextDate: {$lte: today}});
        await Promise.all(
            repeatingTransactions.map(async repeating => {
                let transaction = repeating.toObject();
                let nextDate = calcNextDate(repeating);
                await createTransactionFromData(transaction.transaction, transaction.user);

                return RepeatingTransaction.findByIdAndUpdate(transaction._id, {nextDate});
            })
        );
    }
};

const createRepeatingTransactionFromData = (repeating, transaction) => {
    repeating.user = transaction.user;
    repeating.nextDate = calcNextDate(repeating);
    delete repeating._id;

    let newRepeating = new RepeatingTransaction(repeating);
    return newRepeating.save();
};

const calcNextDate = (repeating) => {
    return moment(repeating.nextDate).startOf('day').add(1, repeating.period).toDate();
};

module.exports = RepeatingTransactionController;