'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const User = require('../models/user');

//TODO: check if it is appropriate to use controller methods in other methods
const CategoryController = {
    get: req => User.findById(req.user._id).then(user => user.categories),

    create: req => User.findByIdAndUpdate(req.user._id, {$addToSet: {categories: req.body.category}}),

    remove: req => User.findByIdAndUpdate(req.user._id, {$pull: {categories: {name: req.params.name}}})
};

module.exports = CategoryController;