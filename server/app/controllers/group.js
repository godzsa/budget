"use strict";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

const Group = require('../models/group');
const resolveOr = require('../utils/promise').resolveOr;

const GroupController = {
    create: req => {
        let group = new Group({users: [req.user._id]});
        return group.save();
    },

    //TODO: WHAT IF WE ARE INVITED TO MORE GROUPS??
    join: req => {
        return Group.findOneAndUpdate(
            {invitations: req.user.email},
            {$addToSet: {users: req.user._id}, $pull: {invitations: req.user.email}})
    },

    leave: async req => {
        try {
            let group = await Group.findOneAndUpdate({users: req.user._id}, {$pull: {users: req.user._id}});
            if (group.users.length < 2) {
                return Group.findByIdAndRemove(group.id);
            }
            return Promise.resolve(true);
        } catch (e) {
            return Promise.reject(false);
        }
    },

    invite: req => {
        return Group.findOneAndUpdate({users: req.user._id}, {$addToSet: {invitations: req.body.email}})
    },

    revokeInvitation: req => {
        return Group.findOneAndUpdate(
            {invitations: req.params.email},
            {$pull: {invitations: req.params.email}})
    },

    get: async req => {
        try {
            let group = await Group.findOne({users: req.user._id}).populate('users');
            if (group && group !== []) {
                return groupToDataPromise(group);
            }
            let found = await Group.findOne({invitations: req.user.email});
            return resolveOr(found != null,
                {hasInvitation: true, hasGroup: false},
                {hasGroup: false, hasInvitation: false});
        } catch (err) {
            return Promise.reject(err);
        }
    }
};

const groupToDataPromise = group => {
    return Promise.resolve({
        users: group.users,
        invitations: group.invitations,
        hasInvitation: false,
        hasGroup: true
    });
};

module.exports = GroupController;