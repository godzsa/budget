
Az alkalmazást három módon használhatjuk Andorid eszközeinken
---------------------------

Elérhető a Play Store-ban:

1. Nyissuk meg a következő linket
- https://play.google.com/store/apps/details?id=com.ionicframework.budget708825
2. Telepítsük

---------------------------

Felvihetjük a mellékelt fájl, és telepítés után használhatjuk

---------------------------

Nyissuk meg a http://thebudget.cloud oldalt, ahol böngészőből használhatjuk a szoftvert
