'use strict';
import {Transaction} from "./transaction";
import {OfflineQueueable} from "./offline-queueable";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

export class RepeatingTransaction implements OfflineQueueable {
  _id: string;
  nextDate: Date;
  transaction: Transaction;
  period: string;
  budgetId: string;

  constructor(opts: any) {
    this._id = opts._id || '';
    this.nextDate = opts.nextUpdate || new Date();
    this.transaction = opts.transaction || new Transaction({});
    this.period = opts.period || 'month';

    this.budgetId = opts.budgetId || "r_" + Date.now() + "_" + Math.round(Math.random() * 10000);
  }

}
