'use strict';
import {Category} from "./category";
import {User} from "./user";
import {OfflineQueueable} from "./offline-queueable";

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2017.02.05
 */

export class Transaction implements OfflineQueueable {
  _id: string;
  amount: number;
  category: Category;
  comment: string;
  currency: string;
  date: Date;
  user: User;
  budgetId: string;

  constructor(opts: any) {
    this._id = opts._id || undefined;
    this.amount = opts.amount || 0;
    this.category = opts.category || new Category({});
    this.comment = opts.comment || '';
    this.currency = opts.currency || 'HUF';
    this.date = opts.date || new Date();
    this.user = opts.user;

    this.budgetId = opts.budgetId || "t_" + Date.now() + "_" + Math.round(Math.random() * 10000);
  }

}
