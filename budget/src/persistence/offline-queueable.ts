'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

export interface OfflineQueueable {
  budgetId: string;
}
