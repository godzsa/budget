'use strict';
import {Category} from "./category";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

export class Saving {
  _id: string;
  name: string;
  start: Date;
  end: Date;
  info: SavingInfo;
  categories: SavingCategory[];

  constructor(opt: any) {
    this._id = opt._id || '';
    this.name = opt.name || '';
    this.start = opt.start || new Date();
    this.end = opt.end || new Date();
    this.categories = opt.categories || [];
    this.info = opt.info || new SavingInfo({});
  }
}


export class SavingInfo {
  spent: number;
  saved: number;
  optimal: number;
  target: number;
  normalTarget: number;

  constructor(opt: any) {
    this.spent = opt.spent || 0;
    this.saved = opt.saved || 0;
    this.optimal = opt.optimal || 0;
    this.target = opt.target || 0;
    this.normalTarget = opt.normalTarget || 0;
  }

  add(info: SavingInfo) {
    this.spent += info.spent;
    this.saved += info.saved;
    this.optimal += info.optimal;
    this.target += info.target;
    this.normalTarget += info.normalTarget;
  }
}

export class SavingCategory {
  category: Category;
  avg: number;
  spent: number;
  percent: number;
  info: SavingInfo;

  constructor(opt: any) {
    this.category = opt.category || new Category({});
    this.avg = opt.avg || 0;
    this.spent = opt.spent || 0; //TODO : is in the info too!!!
    this.percent = opt.percent || 100;
    this.info = opt.info || new SavingInfo({});
  }

}
