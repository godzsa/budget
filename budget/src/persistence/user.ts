'use strict';

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */


//use partial when available
export class User {
  username: string;
  password: string;
  name: string;
  email: string;
  token: string;
  _id: string;

  constructor(opts: any) {
    this.password = opts.password || '';
    this.name = opts.name || '';
    this.email = opts.email || '';
    this.token = opts.token || '';
    this.username = opts.username || '';
    this._id = opts._id;
  }

}
