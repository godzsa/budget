'use strict';

/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2017.02.05.
 */


export class Category {
  name: string;
  color: string;
  icon: string;
  subcategory: string;

  constructor(opt: any) {
    this.color = opt.color || 'white';
    this.icon = opt.icon || '';
    this.subcategory = opt.subcategory || '';
    this.name = opt.name || '';
  }
}
