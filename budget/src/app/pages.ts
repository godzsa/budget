'use strict';
import {SavingsPage} from "../viewmodel/savings/savings";
import {GroupPage} from "../viewmodel/group";
import {StatisticsPage} from "../viewmodel/statistics";
import {RepeatingTransactionsPage} from "../viewmodel/transactions/repeating";
import {TransactionsPage} from "../viewmodel/transactions/transactions";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

export const onlinePages: Array<{ title: string, component: any }> = [
  {title: 'Transactions', component: TransactionsPage},
  {title: 'Repeating Transactions', component: RepeatingTransactionsPage},
  {title: 'Statistics', component: StatisticsPage},
  {title: 'Group', component: GroupPage},
  {title: 'Savings', component: SavingsPage}
];
export const offlinePages: Array<{ title: string, component: any }> = [
  {title: 'Transactions', component: TransactionsPage}
];

