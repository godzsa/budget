import {Component, ViewChild} from "@angular/core";
import {Events, MenuController, Nav, Platform} from "ionic-angular";
import {LoginPage} from "../viewmodel/auth/login";
import {AuthService} from "../model/services/auth";
import {TransactionsPage} from "../viewmodel/transactions/transactions";
import {offlinePages, onlinePages} from "./pages";
import {MessageService} from "../model/services/message";
import {SplashScreen} from "@ionic-native/splash-screen";

@Component({
  templateUrl: 'app.html'
})
export class AppComponent {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{ title: string, component: any }> = onlinePages;


  constructor(private platform: Platform,
              private authService: AuthService,
              private menu: MenuController,
              private events: Events,
              private message: MessageService,
              private splashScreen: SplashScreen) {
    this.splashScreen.show();
    this.initializeApp();
    this.pages = onlinePages;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.authService.login()
        .subscribe(
          success => this.rootPage = TransactionsPage,
          error => this.rootPage = LoginPage,
          () => this.splashScreen.hide()
        );
    });

    this.events.subscribe('logout', () => {
      this.events.publish('error', {message: 'You have been logged out'});
      this.logout()
    });

    this.events.subscribe('offline', (message) => {
      this.events.publish('error', message);
      this.pages = offlinePages;
    });

    this.events.subscribe('online', (message) => {
      this.events.publish('success', message);
      this.pages = onlinePages;
    });
  }

  openPage(page) {
    this.menu.close();
    this.nav.setRoot(page);
  }

  logout() {
    this.authService.logout();
    this.openPage(LoginPage);
    location = location;
  }
}
