import {ErrorHandler, NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {ReactiveFormsModule} from "@angular/forms";
import {ChartsModule} from "ng2-charts";
import {AppComponent} from "./app.component";
import {LoginPage} from "../viewmodel/auth/login";
import {RegisterPage} from "../viewmodel/auth/register";
import {TransactionsPage} from "../viewmodel/transactions/transactions";
import {TransactionDetailPage} from "../viewmodel/transactions/transaction-detail";
import {CategoryComponent} from "../viewmodel/components/category";
import {StatisticsPage} from "../viewmodel/statistics";
import {AuthService} from "../model/services/auth";
import {TransactionService} from "../model/services/transaction";
import {ServerService} from "../model/services/server";
import {StorageService} from "../model/services/storage";
import {LineStats} from "../viewmodel/stats/line-stats";
import {PieStats} from "../viewmodel/stats/pie-stats";
import {GroupPage} from "../viewmodel/group";
import {CategoryDetailPage} from "../viewmodel/category-detail";
import {MessageService} from "../model/services/message";
import {SavingsPage} from "../viewmodel/savings/savings";
import {ShowSavingsPage} from "../viewmodel/savings/show";
import {CreateSavingsPage} from "../viewmodel/savings/create";
import {SavingsBarComponent} from "../viewmodel/components/savings-bar";
import {RepeatingTransactionsPage} from "../viewmodel/transactions/repeating";
import {BaseStats} from "../viewmodel/stats/base-stats";
import {InlineDirective} from "../directives/inline";
import {IonicStorageModule} from "@ionic/storage";
import {ColorPicker} from "../viewmodel/components/color-picker";
import {IconPicker} from "../viewmodel/components/icon-picker";
import {SyncService} from "../model/services/sync";
import {SplashScreen} from "@ionic-native/splash-screen";

@NgModule({
  declarations: [
    AppComponent,
    TransactionsPage,
    TransactionDetailPage,
    RepeatingTransactionsPage,
    LoginPage,
    RegisterPage,
    InlineDirective,
    CategoryComponent, SavingsBarComponent, ColorPicker, IconPicker,
    StatisticsPage,
    GroupPage,
    CategoryDetailPage,
    SavingsPage, ShowSavingsPage, CreateSavingsPage,
    StatisticsPage, PieStats, LineStats, BaseStats
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ChartsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(AppComponent),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AppComponent,
    TransactionsPage,
    TransactionDetailPage,
    RepeatingTransactionsPage,
    LoginPage,
    RegisterPage,
    CategoryComponent, SavingsBarComponent, ColorPicker, IconPicker,
    StatisticsPage,
    GroupPage,
    CategoryDetailPage,
    SavingsPage, ShowSavingsPage, CreateSavingsPage,
    StatisticsPage, PieStats, LineStats, BaseStats
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SplashScreen,
    StorageService,
    ServerService,
    AuthService,
    TransactionService,
    MessageService,
    SyncService,
  ]
})
export class AppModule {
}
