'use strict';

import {Component, EventEmitter, Output} from "@angular/core";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'color-picker',
  templateUrl: '../../view/components/color-picker.html'
})
export class ColorPicker {
  r: number;
  g: number;
  b: number;
  // private a;

  @Output()
  colorChanged = new EventEmitter();

  constructor() {
    this.r = this.g = this.b = 255;
  }

  change() {
    this.colorChanged.emit(this.createColors());
  }

  private createColors() {
    return {
      rgbValues: [this.r, this.g, this.b],
      // rgbaValues: [this.r, this.g, this.b, this.a],
      rgb: `rgb(${this.r},${this.g},${this.b})`,
      // rgba: `rgb(${this.r},${this.g},${this.r},${this.a})`,
      hex: "#" + this.r.toString(16) + this.g.toString(16) + this.b.toString(16)
    }
  }
}
