'use strict';
import {Component, EventEmitter, Input, Output} from "@angular/core";
import {ionIcons} from "../../model/utils/icons";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'icon-picker',
  templateUrl: '../../view/components/icon/icon-picker.html'
})
export class IconPicker {
  icons: string[] = ionIcons;
  selected: string;

  @Input()
  color: string;

  @Output()
  iconChanged = new EventEmitter();

  constructor() {
    this.selected = "";
  }

  selectIcon(icon: string) {
    this.selected = icon;
    this.iconChanged.emit({name: icon});
  }
}
