'use strict';
import {Component, Input} from "@angular/core";
import {SavingInfo} from "../../persistence/saving";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'savings-bar',
  templateUrl: '../../view/components/savings-bar.html'
})
export class SavingsBarComponent {
  @Input()
  saving: SavingInfo;

  @Input()
  colors: { spent: string, optimal: string, target: string, background: string, danger: string };

  values: { saved: string, optimal: string, spent: string, target: string };

  isOverSpent: boolean;

  ngOnInit() {
    this.values = {saved: "", optimal: "", spent: "", target: ""};
    this.isOverSpent = Math.abs(this.saving.spent) > Math.abs(this.saving.optimal);
    this.setValues();
    this.setColors();
  }

  private setValues() {
    let target = (this.saving.target / this.saving.normalTarget * 100 ) + "%";
    let saved = (this.saving.saved / this.saving.normalTarget * 100) + "%";
    let optimal = (this.saving.optimal / this.saving.normalTarget * 100) + "%";
    let spent = (this.saving.spent / this.saving.normalTarget * 100) + "%";
    this.values = {saved, optimal, spent, target};
  }

  private setColors() {
    if (this.colors == null) {
      this.colors = {spent: '#67FF8B', optimal: '#36B254', background: 'whitesmoke', danger: 'tomato', target: '#ddf'};
    }
    if (this.isOverSpent) {
      this.colors.optimal = this.colors.spent;
      this.colors.spent = this.colors.danger;
    }

  }
}
