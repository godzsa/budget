 'use strict';
import {Component, Input, OnInit} from "@angular/core";
import {Category} from "../../persistence/category";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'category',
  templateUrl: '../../view/components/category/category.html'
})
export class CategoryComponent implements OnInit {
  @Input()
  category: Category;

  @Input()
  scale: string;

  transform: string;

  constructor() {
  }

  ngOnInit() {
    if (this.scale == null) {
      this.transform = 'scale(1)';
    } else {
      this.transform = 'scale(' + this.scale + ')';
    }
  }
}
