'use strict';
import {Component} from "@angular/core";
import {SavingsService} from "../../model/services/savings";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Events, NavController} from "ionic-angular";
import * as moment from 'moment';
import {Saving} from "../../persistence/saving";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'create-savings',
  templateUrl: '../../view/pages/savings/create.html',
  providers: [SavingsService]
})
export class CreateSavingsPage {
  saving: Saving;
  savingForm: FormGroup;
  sumSaving: number;

  constructor(private savingsService: SavingsService,
              private formBuilder: FormBuilder,
              private nav: NavController,
              private events: Events) {
  }

  ngOnInit() {
    this.sumSaving = 0;
    this.saving = new Saving({});
    this.savingsService.getDataForNew()
      .subscribe(saving => {
        this.saving = saving
      });

    this.savingForm = this.formBuilder.group({
      'name': ['', Validators.compose([Validators.required, Validators.pattern(/^[^<>{}()\[\]\-\=]*$/)])],
      'start': [moment().format('YYYY-MM-DD').toString(), Validators.required],
      'end': [moment().format('YYYY-MM-DD').toString(), Validators.required]
    });
  }

  startDate():number {
    return new Date().getFullYear();
  }

  endDate(start):string {
    let date = start ? start.getValue() : new Date().getFullYear();
    return moment(date).subtract(1, 'month').format('YYYY-MM-DD'); //because of inconsistence in JS date and moment
  }

  submit() {
    let saving: Saving = Object.assign(this.saving, this.savingForm.getRawValue());

    this.savingsService.create(saving).subscribe(
      saving => this.endEditing(saving),
      error => this.events.publish('error', error)
    );
  }

  calcSavings() {
    this.saving.start = this.getDateFromForm('start');
    this.saving.end = this.getDateFromForm('end');
    this.saving = SavingsService.calcSaving(this.saving);
  }

  private getDateFromForm(name: string) {
    return moment(this.savingForm.getRawValue()[name]).startOf('day').toDate();
  }

  private endEditing(saving: any) {
    this.events.publish('saving:created', saving);
    this.nav.pop();
  }
}
