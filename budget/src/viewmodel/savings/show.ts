'use strict';
import {NavParams} from "ionic-angular";
import {Component} from "@angular/core";
import {Saving} from "../../persistence/saving";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'show-savings',
  templateUrl: '../../view/pages/savings/show.html'
})
export class ShowSavingsPage {
  saving: Saving;

  constructor(private navParams: NavParams) {
  }

  ngOnInit() {
    this.saving = this.navParams.get('saving');
  }

}
