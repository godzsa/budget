'use strict';
import {Events, NavController} from "ionic-angular";
import {Component} from "@angular/core";
import {CreateSavingsPage} from "./create";
import {ShowSavingsPage} from "./show";
import {SavingsService} from "../../model/services/savings";
import {Saving} from "../../persistence/saving";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'savings-list',
  templateUrl: '../../view/pages/savings/list.html',
  providers: [SavingsService]
})
export class SavingsPage {
  savings: Saving[];

  constructor(private savingsService: SavingsService, private events: Events, private navCtrl: NavController) {
    events.subscribe('saving:created', this.createdSaving)
  }

  ngOnInit() {
    this.loadSavings();
  }

  loadSavings() {
    this.savingsService.getSavings()
      .subscribe(
        savings => this.savings = savings,
        error => this.events.publish('error', error)
      )
  }

  show(saving: Saving) {
    this.navCtrl.push(ShowSavingsPage, {saving});
  }

  create() {
    this.navCtrl.push(CreateSavingsPage);
  }

  remove(saving: Saving, i: number) {
    this.savingsService.remove(saving).subscribe(
      success => this.savingRemoved(i),
      error => this.events.publish('error', error)
    )
  }

  private createdSaving = (saving: Saving) => {
    this.loadSavings();
  };

  private savingRemoved(i: number) {
    this.savings.splice(i, 1);
    this.events.publish('success', {message: 'Saving removed!'})
  }
}
