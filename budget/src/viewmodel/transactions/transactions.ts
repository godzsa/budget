import {Component} from "@angular/core";
import {Events, NavController, NavParams} from "ionic-angular";
import {Transaction} from "../../persistence/transaction";
import {TransactionService} from "../../model/services/transaction";
import {TransactionDetailPage} from "./transaction-detail";
import {User} from "../../persistence/user";
import {StatisticsPage} from "../statistics";
import * as _ from "lodash";


@Component({
  selector: 'transaction-list',
  templateUrl: '../../view/pages/transactions/list.html'
})
export class TransactionsPage {
  public transactions: Transaction[];
  private batch: number;
  public listType: string = 'personal';
  public user: User;

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private transactionService: TransactionService,
              private events: Events) {
    events.subscribe('transaction:edited', () => this.loadTransactions());
    events.subscribe('transaction:added', () => this.loadTransactions());
    events.subscribe('transaction:synced', () => {
      events.publish('online', {message: 'Welcome back!'});
      this.loadTransactions()
    });
  }

  ngOnInit() {
    this.user = this.navParams.get('user') || null;
    this.loadTransactions();
  }

  refresh(refresher) {
    this.transactionService.update()
      .then(() => refresher.complete());
  }

  listTypeChanged() {
    this.loadTransactions();
  }

  createTransaction() {
    this.navCtrl.push(TransactionDetailPage, {transaction: new Transaction({})});
  }

  editTransaction(transaction: Transaction) {
    this.navCtrl.push(TransactionDetailPage, {transaction: transaction, editing: true});
  }

  remove(transaction: Transaction, i: number) {
    this.transactionService.remove(transaction)
      .subscribe(success => this.transactions.splice(i, 1));
  }

  showStats() {
    this.navCtrl.push(StatisticsPage, {user: this.user});
  }

  infiniteScrollUpdate(infiniteScroll) {
    this.nextBatch().subscribe(
      transactions => {
        this.addTransactions(transactions);
        if (infiniteScroll.complete) {
          infiniteScroll.complete()
        }
      },
      error => this.events.publish('error', {message: 'Could not download transactions!'})
    );
  }

  private addTransactions = (transactions: Transaction[] | Transaction) => {
    if (Array.isArray(transactions)) {
      this.transactions = _.unionWith(this.transactions, transactions, _.isEqual);
    } else {
      this.transactions = _.unionWith(this.transactions, [transactions], _.isEqual);
    }
  };


  private loadTransactions() {
    this.batch = -1;
    this.transactions = [];
    this.nextBatch().subscribe(
      transactions => this.addTransactions(transactions),
      error => this.events.publish('error', {message: 'Could not download transactions!'})
    );
  };

  private nextBatch() {
    this.batch++;

    if (this.user != null) {
      return this.transactionService.nthBatchForUser(this.batch, this.user);
    } else if (this.listType === 'group') {
      return this.transactionService.nthBatchGroup(this.batch);
    }

    return this.transactionService.nthBatch(this.batch);
  }
}
