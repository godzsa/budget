import {Component} from "@angular/core";
import {Events, NavController} from "ionic-angular";
import {TransactionService} from "../../model/services/transaction";
import {TransactionDetailPage} from "./transaction-detail";
import {RepeatingTransaction} from "../../persistence/repeating";


@Component({
  selector: 'repeating-transaction-list',
  templateUrl: '../../view/pages/transactions/repeating.html',
})
export class RepeatingTransactionsPage {
  transactions: RepeatingTransaction[];

  constructor(public navCtrl: NavController, private transactionService: TransactionService, private events: Events) {
    events.subscribe('transaction:edited', () => this.refreshPage());
    events.subscribe('transaction:added', () => this.refreshPage());
  }

  ngOnInit() {
    this.transactions = [];
    this.refreshPage();
  }

  create() {
    this.navCtrl.push(TransactionDetailPage, {transaction: new RepeatingTransaction({})});
  }

  edit(repeating: RepeatingTransaction) {
    this.navCtrl.push(TransactionDetailPage, {transaction: new RepeatingTransaction(repeating), editing: true});
  }

  remove(repeating: RepeatingTransaction) {
    this.transactionService.remove(new RepeatingTransaction(repeating))
      .subscribe(success => {
        this.events.publish('success', {message: 'Transaction removed!'});
        this.refreshPage();
      });
  }

  private refreshPage() {
    this.transactionService.repeating().subscribe(
      transactions => this.transactions = transactions,
      error => this.events.publish('error', {message: 'Could not download transactions!'})
    );
  }
}
