import {Component} from "@angular/core";
import {TransactionService} from "../../model/services/transaction";
import {Events, ModalController, NavController, NavParams} from "ionic-angular";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Transaction} from "../../persistence/transaction";
import {Observable} from "rxjs";
import {Category} from "../../persistence/category";
import {CategoryService} from "../../model/services/category";
import {CategoryDetailPage} from "../category-detail";
import * as moment from "moment";
import * as _ from "lodash";
import {RepeatingTransaction} from "../../persistence/repeating";

/**
 * This component handles the transaction, repeating transaction edition/creation
 */

@Component({
  providers: [CategoryService],
  selector: 'transaction-detail',
  templateUrl: '../../view/pages/transactions/detail.html',
})
export class TransactionDetailPage {
  private editing: boolean;
  private detail: Transaction;
  transaction: FormGroup;
  selectedCategory: Category;
  categories: Category[];
  isRepeating: string;
  period: string;

  newCategoryButton: Category = new Category({name: 'create', color: 'black', icon: 'add', subcategory: '-'});

  constructor(private transactionService: TransactionService,
              private nav: NavController,
              private formBuilder: FormBuilder,
              private navParams: NavParams,
              private categoryService: CategoryService,
              public modalCtrl: ModalController,
              private events: Events) {
  }

  ngOnInit() {
    this.setFieldsFromNavParams();
    this.buildForm();

    this.categoryService.categories()
      .subscribe(categories => this.setCategories(categories));
  }

  private setCategories(categories) {
    this.categories = categories;
    let selectedCategory = this.detail.category ? _.find(categories, this.detail.category) : undefined;

    if (selectedCategory == null && this.editing) {
      selectedCategory = this.detail.category;
      this.categories.unshift(selectedCategory);
    }
    this.selectedCategory = selectedCategory;
  }

  submit() {
    let transaction = new Transaction(this.transaction.getRawValue());
    transaction.category = this.selectedCategory;

    this.createSubmitter(transaction).subscribe(
      success => this.endEditing(transaction),
      error => this.events.publish('error', error)
    );
  }

  getCategories(): Category[] {
    return this.categories;
  }

  selectCategory(category: Category) {
    this.selectedCategory = category;
    this.transaction.controls['category'].setValue(category);
  }

  newCategory() {
    let categoryModal = this.modalCtrl.create(CategoryDetailPage, undefined, {enableBackdropDismiss: false});
    categoryModal.onDidDismiss(
      category => this.categoryService.create(new Category(category)).subscribe(success => this.categories.push(category))
    );
    categoryModal.present();
  }

  private setFieldsFromNavParams() {
    this.editing = this.navParams.get('editing');
    let transaction = this.navParams.get('transaction');

    let isRepeating = transaction instanceof RepeatingTransaction;
    this.isRepeating = isRepeating ? 'repeating' : 'single';
    if (isRepeating) {
      this.detail = transaction.transaction;
      this.period = transaction.period || 'month';
      this.editing = transaction._id; //TODO THIS STINKS
    } else {
      this.detail = transaction;
    }
    //transation is created with dummy category, we want to force the user to select a category with validation
    if (!this.editing) {
      this.detail.category = undefined;
    }
  }

  private buildForm() {
    this.transaction = this.formBuilder.group({
      amount: [this.detail.amount, Validators.compose([Validators.required, Validators.pattern(/^[+-]?[1-9][0-9]*$/)])],
      currency: [{value: 'HUF', disabled: true}],
      date: [moment(this.detail.date).format("YYYY-MM-DD"), Validators.required],
      comment: [this.detail.comment, Validators.pattern(/^[^<>{}()\[\]\-\=]*$/)],
      category: [this.detail.category, Validators.required],
      _id: [this.detail._id],
      budgetId: [this.detail.budgetId]
    });
  }

  private endEditing(transaction: Transaction | RepeatingTransaction) {
    if (this.editing) {
      this.events.publish('transaction:edited', transaction);
    } else {
      this.events.publish('transaction:added', transaction);
    }
    this.nav.pop();
  }

  private createSubmitter(transaction: Transaction): Observable<any> {
    let data = this.isRepeating === 'repeating' ? this.createRepeatingTransaction(transaction) : transaction;

    if (this.editing) {
      return this.transactionService.edit(data);
    }
    return this.transactionService.create(data);
  }

  private createRepeatingTransaction(transaction: Transaction): RepeatingTransaction {
    return new RepeatingTransaction({
      _id: this.editing,
      transaction: transaction,
      period: this.period,
      nextDate: transaction.date
    });
  }


}
