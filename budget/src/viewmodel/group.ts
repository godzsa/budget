'use strict';
import {Component} from "@angular/core";
import {GroupService} from "../model/services/group";
import {Events, NavController} from "ionic-angular";
import {TransactionsPage} from "./transactions/transactions";
import {User} from "../persistence/user";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'group',
  templateUrl: '../view/pages/group/group.html',
  providers: [GroupService]
})
export class GroupPage {
  group: {
    hasGroup: boolean,
    hasInvitation: boolean,
    users: User[],
    invitations: User[],
  };

  constructor(private groupService: GroupService, private navCtrl: NavController, private events: Events) {
    this.group = {hasGroup: false, hasInvitation: false, users: [], invitations: []};
  }

  ngOnInit() {
    this.refreshPage();
  }

  seeTransactions(user: User) {
    this.navCtrl.push(TransactionsPage, {user: user});
  }

  join() {
    this.groupService.joinGroup().subscribe(
      succ => this.refreshPage(),
      err => this.events.publish('error', {message: 'Could not join the group!'})
    )
  }

  leave() {
    this.groupService.leaveGroup().subscribe(
      succ => this.refreshPage(),
      err => this.events.publish('error', {message: 'There was an error while leaving group!'})
    )
  }

  create() {
    this.groupService.createGroup().subscribe(
      succ => this.refreshPage(),
      err => this.events.publish('error', {message: 'Could not create group!'})
    );
  }

  invite(email: string) {
    let invitee = new User({email: email});
    this.groupService.invite(invitee).subscribe(
      succ => this.refreshPage(),
      err => this.events.publish('error', {message: 'Invitation failed!'})
    );
  }

  revokeInvitation(email: string) {
    let invitee = new User({email: email});
    this.groupService.revokeInvitation(invitee).subscribe(
      succ => this.refreshPage(),
      err => this.events.publish('error', {message: 'Could not revoke invitation failed!'})
    );
  }

  refreshPage() {
    this.groupService.getGroup().subscribe(
      group => this.group = group,
      err => this.events.publish('error', {message: 'Could not load group data!'})
    );
  }

}
