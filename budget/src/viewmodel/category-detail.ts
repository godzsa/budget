'use strict';
import {Component} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Category} from "../persistence/category";
import {ViewController} from "ionic-angular";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'category-detail',
  templateUrl: '../view/pages/categories/detail.html',
})
export class CategoryDetailPage {
  category: FormGroup;
  preview: Category;

  constructor(private formBuilder: FormBuilder,
              private viewCtrl: ViewController) {

    this.preview = new Category({icon: "", color: "white"});

    this.category = formBuilder.group({
      name: ["", Validators.required],
      color: ["", Validators.required],
      icon: ["", Validators.required],
      subcategory: [""]
    });
  }

  submit() {
    let category = new Category(this.category.getRawValue());
    this.viewCtrl.dismiss(category);
  }

  changeColor(color) {
    this.category.controls['color'].setValue(color.rgb);
    this.preview.color = color.rgb;
  }

  changeIcon(icon) {
    this.category.controls['icon'].setValue(icon.name);
    this.preview.icon = icon.name;
  }
}
