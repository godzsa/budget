'use strict';

import {Component} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Events, MenuController, NavController} from "ionic-angular";
import {User} from "../../persistence/user";
import {AuthService} from "../../model/services/auth";
import {TransactionsPage} from "../transactions/transactions";


@Component({
  selector: 'register-page',
  templateUrl: '../../view/pages/login/register.html'
})
export class RegisterPage {
  newUser: FormGroup;

  constructor(public navCtrl: NavController,
              private formBuilder: FormBuilder,
              public authService: AuthService,
              private events: Events,
              private menu: MenuController) {
    this.newUser = formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern(/^[\w\d._-]{3,}$/)])],
      name: ['', Validators.compose([Validators.required, Validators.pattern(/^[\w\d ._-]{3,}$/)])],
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.pattern(/^[^<>{}()\[\]\-\=]{3,}$/)])]
    });
  }

  register() {
    let user = new User(this.newUser.getRawValue());

    this.authService.register(user)
      .subscribe(
        success => this.navCtrl.setRoot(TransactionsPage),
        error => this.events.publish('error', {message: 'There must be a problem with your connection!'})
      );
  }

  ionViewWillEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }


}
