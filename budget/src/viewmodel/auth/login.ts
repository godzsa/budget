'use strict';

import {Component} from "@angular/core";
import {MenuController, NavController} from "ionic-angular";
import {User} from "../../persistence/user";
import {AuthService} from "../../model/services/auth";
import {TransactionsPage} from "../transactions/transactions";
import {RegisterPage} from "./register";


@Component({
  selector: 'login-page',
  templateUrl: '../../view/pages/login/login.html'
})
export class LoginPage {
  user: User;

  constructor(public navCtrl: NavController, public authService: AuthService, public menu: MenuController) {
    this.user = new User({});
  }

  ionViewWillEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }

  login() {
    this.authService.login(this.user)
      .subscribe(
        user => this.navCtrl.setRoot(TransactionsPage),
        error => this.user = new User({})
      );
  }

  registerPage() {
    this.navCtrl.push(RegisterPage);
  }

}
