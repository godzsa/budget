'use strict';
import {StatisticsService} from "../../model/services/statistics";
import {Component, OnInit, ViewChild} from "@angular/core";
import {Navbar, NavController, NavParams} from "ionic-angular";
import {User} from "../../persistence/user";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'base-stats',
  providers: [StatisticsService],
  template: '<h1>Base stats is abstract</h1>'
})
export class BaseStats implements OnInit {
  public interval: string;
  public stats: any;
  public chartType: string;
  public listType: string;
  public user: User;

  constructor(public statService: StatisticsService, private navParam: NavParams, public nav: NavController) {
    this.setup();
  }

  @ViewChild(Navbar) navBar: Navbar;

  ngOnInit() {
    this.user = this.navParam.get('user');
    this.listType = 'personal';
    this.getData(this.interval);
  }


  setup() {
    console.log('ERROR: Abstract setup called!');
  }

  getData(interval: any) {
    console.log('ERROR: Abstract getData called!');
  }


  isUserSet() {
    return this.user != null;
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      this.nav.parent.viewCtrl.dismiss();
    };
  }

  listTypeChanged() {
    this.getData(this.interval);
  }
}
