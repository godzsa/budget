'use strict';
import {Component} from "@angular/core";
import {BaseStats} from "./base-stats";
import {StatisticsService} from "../../model/services/statistics";
import * as moment from 'moment';
import {User} from "../../persistence/user";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
@Component({
  selector: 'line-stats',
  providers: [StatisticsService],
  templateUrl: '../../view/pages/statistics/line-stats.html'
})
export class LineStats extends BaseStats {
  setup() {
    this.chartType = 'line';
    this.interval = 'month';
  }

  getData() {
    this.getLineDataForInterval(this.interval)
      .subscribe(line => {
        this.stats = line;
      });
  }

  private getLineDataForInterval(interval: any) {
    if (this.user) {
      return this.statService.getLineChartData(interval, new User(this.user));
    }
    return this.statService.getLineChartData(interval, this.listType)
  }

  statisticLegend(start:Date) {
    if (this.interval === 'week') {
      return moment(start).startOf('week').format('Do') + " - " + moment(start).endOf('week').format('Do MMMM, YYYY');
    }
    if (this.interval === 'month') {
      return moment(start).format('MMMM, YYYY');
    }
    if (this.interval === 'year') {
      return moment(start).format("YYYY");
    }
  }


}
