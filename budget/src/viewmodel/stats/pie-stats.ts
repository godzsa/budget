'use strict';
import {Component} from "@angular/core";
import {BaseStats} from "./base-stats";
import {StatisticsService} from "../../model/services/statistics";
import {User} from "../../persistence/user";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'pie-stats',
  providers: [StatisticsService],
  templateUrl: '../../view/pages/statistics/pie-stats.html'
})
export class PieStats extends BaseStats {
  setup() {
    this.chartType = 'doughnut';
    this.interval = 'month';
  }

  getData() {
    this.getPieDataForInterval(this.interval)
      .subscribe(data => this.stats = data);
  }

  private getPieDataForInterval(interval: any) {
    if (this.user) {
      return this.statService.getPieChartData(interval, new User(this.user));
    }
    return this.statService.getPieChartData(interval, this.listType)
  }

  sum(array: any[]) {
    return array.reduce((a, b) => a + b, 0);
  }

  abs(array: any[]) {
    return array.map(item => Math.abs(item));
  }
}


