'use strict';
import {Component} from "@angular/core";
import {NavParams} from "ionic-angular";
import {PieStats} from "./stats/pie-stats";
import {LineStats} from "./stats/line-stats";
import {User} from "../persistence/user";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Component({
  selector: 'statistics',
  templateUrl: '../view/pages/statistics/tabs.html',
})
export class StatisticsPage {
  interval: string;

  pieStats: any;
  lineStats: any;
  user: User;

  constructor(private navParam: NavParams) {
    this.pieStats = PieStats;
    this.lineStats = LineStats;
  }

  ngOnInit() {
    this.user = this.navParam.get('user');
  }

}
