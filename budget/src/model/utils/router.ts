/**
 * Created by david on 2017.02.17..
 */

//TODO: think about a better filename
export enum Method { GET, CREATE, EDIT, REMOVE, POST}

export class Routes {
  static LOGIN: string = '/auth/login';
  static REGISTER: string = '/auth/register';

  static TRANSACTION: string = '/transactions';
  static REPEATING_TRANSACTION: string = '/repeating';
  static REPEATING_TRANSACTION_GET: string = '/repeating/get'; //hack
  static TRANSACTION_BATCH: string = '/transactions/batch';

  static TRANSACTION_GROUP_BATCH: string = '/transactions/group/batch';
  static PING: string = '/ping';
  static GROUP: string = '/groups';
  static GROUP_JOIN: string = '/groups/join';

  static GROUP_INVITE: string = '/groups/invitation';

  static CATEGORY: string = '/category';
  static SAVINGS: string = '/savings';

  static SAVINGS_NEW: string = '/savings/new';
  static STATISTICS_PIE: string = '/statistics/pie';
  static STATISTICS_PIE_GROUP: string = '/statistics/group/pie';
  static STATISTICS_LINE: string = '/statistics/line';
  static STATISTICS_LINE_GROUP: string = '/statistics/group/line';
}
//TODO: rename
export class ServerData {
  static HOST: string = 'localhost';// '10.0.2.2';
  static PORT: string = '8080';
  static prefix: string = `http://${ServerData.HOST}:${ServerData.PORT}`;
}
