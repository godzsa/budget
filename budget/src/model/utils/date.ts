'use strict';
import * as moment from "moment";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

export class DateUtils {
  static getInterval = (nth: number, unit: moment.unitOfTime.Base): { from: Date, to: Date } => {
    let from = moment().subtract(nth, unit).toDate();
    let to = moment().subtract(nth - 1, unit).toDate();

    return {from: from, to: to};
  };

  static dateDifference = (unit, start, end): number => {
    return moment(end).diff(start, unit, true);
  };

  static whereInProgress = (start: Date, end: Date): number => {
    let intervalLength = DateUtils.dateDifference('day', start, end);
    if (intervalLength === 0) return 0;

    let correctEnd = end < new Date() ? end : new Date();
    let daysSinceStart = DateUtils.dateDifference('day', start, correctEnd);
    let progress = daysSinceStart / intervalLength;
    return daysSinceStart < 0 ? 0 : progress;
  }
}
