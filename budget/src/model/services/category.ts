'use strict';

import {Injectable} from "@angular/core";
import {ServerService} from "./server";
import {Method, Routes} from "../utils/router";
import {Category} from "../../persistence/category";
import {Observable} from "rxjs";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
@Injectable()
export class CategoryService {
  static basicCategories: Category[] = [
    new Category({category: 'money', color: 'lime', icon: 'ios-card'}),
    new Category({category: 'transport', color: 'brown', icon: 'ios-car-outline'}),
    new Category({category: 'rent', color: 'lightgoldenrodyellow', icon: 'ios-home-outline'}),
    new Category({category: 'food', color: 'gold', icon: 'ios-pizza-outline'}),
    new Category({category: 'health', color: 'tomato', icon: 'ios-medkit-outline'})
  ];

  constructor(private serverService: ServerService) {
  }

  categories(): Observable<Category[]> {
    return this.serverService.request(Routes.CATEGORY, Method.GET)
      .map(categories => {
        return CategoryService.basicCategories.concat(categories)
      })
      .catch(this.handleError);
  }

  create(category: Category) {
    return this.serverService.request(Routes.CATEGORY, Method.CREATE, true, {category: category})
      .catch(this.handleError);
  }

  remove(category: Category) {
    return this.serverService.request(Routes.CATEGORY, Method.REMOVE, true, {remove: category.name})
      .catch(this.handleError);
  }

  handleError(err): Observable<Category[]> {
    return Observable.of(CategoryService.basicCategories);
  }
}
