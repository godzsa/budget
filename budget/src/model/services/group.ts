'use strict';

import {Injectable} from "@angular/core";
import {ServerService} from "./server";
import {Method, Routes} from "../utils/router";
import {Observable} from "rxjs";
import {User} from "../../persistence/user";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
@Injectable()
export class GroupService {

  constructor(private serverService: ServerService) {
  }

  getGroup(): Observable<any> {
    return this.serverService.request(Routes.GROUP, Method.GET)
  }

  invite(user: User): Observable<boolean> {
    return this.serverService.request(Routes.GROUP_INVITE, Method.POST, true, {email: user.email})
  }

  revokeInvitation(user: User): Observable<boolean> {
    return this.serverService.request(Routes.GROUP_INVITE, Method.REMOVE, true, {remove: user.email})
  }

  createGroup(): Observable<boolean> {
    return this.serverService.request(Routes.GROUP, Method.CREATE)
  }

  joinGroup(): Observable<boolean> {
    return this.serverService.request(Routes.GROUP_JOIN, Method.POST)
  }

  leaveGroup(): Observable<boolean> {
    return this.serverService.request(Routes.GROUP_JOIN, Method.REMOVE, true, {remove: ""})
  }

}
