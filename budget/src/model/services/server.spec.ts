'use strict';
import {ServerService} from "./server";
import {Http, RequestOptions, Response, ResponseOptions} from "@angular/http";
import {MockBackend} from "@angular/http/testing";
import {Observable} from "rxjs/Observable";
import {Method} from "../utils/router";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */


describe('Server Service', () => {
  let http: Http = new Http(new MockBackend(), new RequestOptions());
  let response: Response = new Response(new ResponseOptions());
  let server: ServerService;


  beforeEach(function () {
    server = new ServerService(http);
  });


  describe('GET METHOD', function () {
    it('should respond to get requests with data if server sends some', function (done) {
      let dummyData = {dummy: 'dummyData'};
      spyOn(http, 'get').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({data: dummyData});
      server.request('dummy', Method.GET).subscribe(
        data => {
          expect(data).toEqual(dummyData);
          done();
        });
    });

    it('should respond to get if there is no data but is successful', function (done) {
      spyOn(http, 'get').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({success: true});

      server.request('dummy', Method.GET).subscribe(
        data => {
          expect(data).toEqual(true);
          done();
        }
      );
    });

    it('should respond to with error if the server responds with error', function (done) {
      spyOn(http, 'get').and.returnValue(Observable.throw(new Error('error')));

      server.request('dummy', Method.GET).subscribe(
        success => {
          throw new Error('should not be here');
        },
        err => {
          expect(err).toBeTruthy();
          done();
        }
      );
    });
  });

  describe('CREATE METHOD', function () {
    it('should respond to get requests with data if server sends some', function (done) {
      let dummyData = {dummy: 'dummyData'};
      spyOn(http, 'post').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({data: dummyData});
      server.request('dummy', Method.CREATE).subscribe(
        data => {
          expect(data).toEqual(dummyData);
          done();
        });
    });

    it('should respond to get if there is no data but is successful', function (done) {
      spyOn(http, 'post').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({success: true});

      server.request('dummy', Method.CREATE).subscribe(
        data => {
          expect(data).toEqual(true);
          done();
        }
      );
    });

    it('should respond to with error if the server responds with error', function (done) {
      spyOn(http, 'post').and.returnValue(Observable.throw(new Error('error')));

      server.request('dummy', Method.CREATE).subscribe(
        success => {
          throw new Error('should not be here');
        },
        err => {
          expect(err).toBeTruthy();
          done();
        }
      );
    });
  });


  describe('POST METHOD', function () {
    it('should respond to get requests with data if server sends some', function (done) {
      let dummyData = {dummy: 'dummyData'};
      spyOn(http, 'post').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({data: dummyData});
      server.request('dummy', Method.POST).subscribe(
        data => {
          expect(data).toEqual(dummyData);
          done();
        });
    });

    it('should respond to get if there is no data but is successful', function (done) {
      spyOn(http, 'post').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({success: true});

      server.request('dummy', Method.POST).subscribe(
        data => {
          expect(data).toEqual(true);
          done();
        }
      );
    });

    it('should respond to with error if the server responds with error', function (done) {
      spyOn(http, 'post').and.returnValue(Observable.throw(new Error('error')));

      server.request('dummy', Method.POST).subscribe(
        success => {
          throw new Error('should not be here');
        },
        err => {
          expect(err).toBeTruthy();
          done();
        }
      );
    });
  });

  describe('EDIT METHOD', function () {
    it('should respond to get requests with data if server sends some', function (done) {
      let dummyData = {dummy: 'dummyData'};
      spyOn(http, 'put').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({data: dummyData});
      server.request('dummy', Method.EDIT).subscribe(
        data => {
          expect(data).toEqual(dummyData);
          done();
        });
    });

    it('should respond to get if there is no data but is successful', function (done) {
      spyOn(http, 'put').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({success: true});

      server.request('dummy', Method.EDIT).subscribe(
        data => {
          expect(data).toEqual(true);
          done();
        }
      );
    });

    it('should respond to with error if the server responds with error', function (done) {
      spyOn(http, 'put').and.returnValue(Observable.throw(new Error('error')));

      server.request('dummy', Method.EDIT).subscribe(
        success => {
          throw new Error('should not be here');
        },
        err => {
          expect(err).toBeTruthy();
          done();
        }
      );
    });
  });

  describe('REMOVE METHOD', function () {
    it('should respond to get requests with data if server sends some', function (done) {
      let dummyData = {dummy: 'dummyData'};
      spyOn(http, 'delete').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({data: dummyData});
      server.request('dummy', Method.REMOVE, true, {remove: 'dummy'}).subscribe(
        data => {
          expect(data).toEqual(dummyData);
          done();
        });
    });

    it('should respond to get if there is no data but is successful', function (done) {
      spyOn(http, 'delete').and.returnValue(Observable.of(response));
      spyOn(response, 'json').and.returnValue({success: true});

      server.request('dummy', Method.REMOVE, true, {remove: 'dummy'}).subscribe(
        data => {
          expect(data).toEqual(true);
          done();
        }
      );
    });

    it('should respond to with error if the server responds with error', function (done) {
      spyOn(http, 'delete').and.returnValue(Observable.throw(new Error('error')));

      server.request('dummy', Method.REMOVE, true, {remove: 'dummy'}).subscribe(
        success => {
          throw new Error('should not be here');
        },
        err => {
          expect(err).toBeTruthy();
          done();
        }
      );
    });

    it('should throw error if there is no data.remove configured', function (done) {
      spyOn(http, 'delete').and.returnValue(Observable.throw(new Error('error')));

      try {
        server.request('dummy', Method.REMOVE).subscribe(
          success => {
            throw new Error('should not be here');
          },
          err => {
            throw new Error('should not be here');
          }
        );
      } catch (e) {
        expect(e).toBeTruthy();
        done();
      }
    });
  });

  it('should set auth header', function () {
    server.setAuthHeader('token');
    expect(server.auth.headers.get('Authorization')).toEqual('JWT token');
  });


});
