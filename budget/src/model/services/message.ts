'use strict';

import {Injectable} from "@angular/core";
import {Events, ToastController} from "ionic-angular";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
@Injectable()
export class MessageService {

  constructor(private events: Events, private  toastCtrl: ToastController) {
    events.subscribe('error', (error) => {
        console.log(error);
        let toast = this.toastCtrl.create({
          message: error.message || "There was an error!",
          duration: 3000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: 'close'
        });
        toast.present();
      }
    );
    events.subscribe('success', (success) => {
        let toast = this.toastCtrl.create({
          message: success.message || "Success!",
          duration: 3000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: 'close'
        });
        toast.present();
      }
    );
  }

}
