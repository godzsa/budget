'use strict';
import {StorageService} from "./storage";
import {ServerService} from "./server";
import {Method, Routes} from "../utils/router";
import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {OfflineQueueable} from "../../persistence/offline-queueable";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
//TODO: this is not abstract enough
//  refactor to work with parameters, or use factory pattern
@Injectable()
export class SyncService {

  constructor(private storageService: StorageService, private serverService: ServerService) {
  }

  getData(route: Routes, data: any): Observable<any> {
    return this.serverService.checkConnection()
      .mergeMap(connection => {
        if (connection) {
          return this.serverService.request(route, Method.POST, true, data); //TODO: not abstract
        } else {
          return this.getStoredTransactions(route === Routes.REPEATING_TRANSACTION);
        }
      })
  }

  persist(method: Method, data: any): Observable<any> {
    let route = data.isRepeating ? Routes.REPEATING_TRANSACTION : Routes.TRANSACTION;

    return this.serverService.request(route, method, true, data)
      .catch(err => this.handleServerError(err, data, method));
  }

  async tryToSync(): Promise<any> {
    let connection = await this.serverService.checkConnection().toPromise();
    return connection ? this.sync() : Promise.reject({message: 'Could not synchronize'}); //TODO rejet error
  }

  private sync(): Promise<any> {
    return this.storageService.whenReady(() =>
      this.storageService.get(StorageService.OFFLINE_QUEUE)
        .then(queue => {
          this.storageService.remove(StorageService.OFFLINE_QUEUE);
          let offlineQueue = Array.isArray(queue) ? queue : [];
          this.uploadQueue(offlineQueue, 0);
        })
    );
  }

  private uploadQueue(queue: any, i: number): Promise<any> {
    if (queue.length === i) {
      return Promise.resolve('synced');
    }
    this.persist(Method.CREATE, queue[i]).subscribe(
      () => this.uploadQueue(queue, i + 1)
    )
  }

  private getStoredTransactions(isRepeating: boolean = false): Observable<OfflineQueueable> {
    return Observable.from(this.storageService.whenReady(() => this.storageService.get(StorageService.OFFLINE_QUEUE)))
      .map(transactions => transactions
        .filter(transaction => transaction.isRepeating === isRepeating)
        .map(transaction => transaction.transaction)
      )
  }

  private persistToStorage(method: Method, data: any): Promise<any> {
    return this.storageService.whenReady(() => {
        switch (method) {
          case Method.POST:
          case Method.CREATE:
            return this.storageService.push(StorageService.OFFLINE_QUEUE, data);
          case Method.EDIT:
            return this.storageService.updateWith(StorageService.OFFLINE_QUEUE, data,
              item => item.transaction.budgetId === data.transaction.budgetId);
          case Method.REMOVE:
            return this.storageService.spliceWith(StorageService.OFFLINE_QUEUE,
              item => item.transaction.budgetId === data.remove);
        }
      }
    )
  }
  //TODO: refactor
  private handleServerError(err: Response, data?: any, method?: Method): Observable<any> {
    switch (err.status) {
      case 401:
      // return Observable.of(this.events.publish('logout', {message: 'Your login data was incorrect'}));
      case 400:
        return Observable.throw(new Error('The data provided had errors'));
      case 0:
      case 500:
        return Observable.fromPromise(
          this.persistToStorage(method, data)
        );
    }
    return Observable.throw(new Error('Unknown error'));
  }
}
