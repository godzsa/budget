/**
 * Created by david on 2017.02.17..
 */
import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";


@Injectable()
export class StorageService extends Storage {
  public static OFFLINE_QUEUE = 'offlineQueue';
  public static USER = 'user';

  constructor() {
    super({}); //won't work if we want to have configuration
  }

  whenReady(fn: Function): Promise<any> {
    return this.ready().then(() => fn());
  }

  async push(name: string, data: any): Promise<any> {
    let array = await this.getArray(name);
    array.push(data);
    return this.set(name, array);
  }

  async spliceWith(name: string, fn: (value) => boolean): Promise<any> {
    let array = await this.getArray(name);
    array = array.filter((item) => !fn(item));
    return this.set(name, array)
  }

  async updateWith(name: string, data: any, fn: (value) => boolean): Promise<any> {
    let array = await this.getArray(name);
    let i = array.findIndex(fn);
    array[i] = data;
    return this.set(name, array);
  }

  private async getArray(name: string): Promise<any[]> {
    let array = await this.get(name);
    return array || [];
  }
}
