'use strict';
import {AuthService} from "./auth";
import {Events} from "ionic-angular";
import {StorageServiceMock} from "./storage.mock";
import {ServerServiceMock} from "./server.mock";
import {User} from "../../persistence/user";
import {Observable} from "rxjs/Observable";
import {ServerService} from "./server";
import {StorageService} from "./storage";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */



describe('Auth Service', () => {
  let server: ServerService = new ServerServiceMock();
  let storage: StorageService = new StorageServiceMock();
  let auth: AuthService;
  beforeEach(function () {
    auth = new AuthService(storage, server, new Events());
  });

  it('should log a user in if server returns a token and no errors', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of({token: 'token'}));
    auth.login(new User({}))
      .subscribe(
        success => {
          expect(success).toBeTruthy();
          done();
        }
      )
  });

  it('should log a user in if a token is stored in the storage', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of({token: 'token'}));
    spyOn(storage, 'get').and.returnValue(Promise.resolve({username: 'name', token: 'token'}));
    spyOn(storage, 'whenReady').and.returnValue(Promise.resolve({username: 'name', token: 'token'}));
    auth.login().subscribe(
      success => {
        expect(success).toBeTruthy();
        done();
      }
    );
  });

  it('should throw an error if user data is not correct', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    auth.login(new User({}))
      .subscribe(
        success => {
          throw new Error('shouldnt be here');
        },
        error => {
          expect(error).toBeTruthy();
          done();
        }
      );
  });

  it('should send registration data to server and log user in if data is correct, and store token', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of({token: 'token'}));
    spyOn(storage, 'set').and.returnValue(Promise.resolve('success'));
    auth.register(new User({}))
      .subscribe(
        success => {
          // expect(storage.set).toHaveBeenCalled();
          expect(success).toBeTruthy();
          done();
        }
      );
  });

  it('should return error if not correct and should not log user in', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    auth.register(new User({}))
      .subscribe(
        success => {
          throw new Error('shouldnt be here');
        },
        error => {
          expect(error).toBeTruthy();
          done();
        }
      );
  });

  it('should set auth header in ServerService if user is logged in', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of({token: 'token'}));
    spyOn(server, 'setAuthHeader').and.stub();
    auth.login(new User({}))
      .subscribe(
        success => {
          expect(success).toBeTruthy();
          expect(server.setAuthHeader).toHaveBeenCalled();
          done();
        }
      );
  });
});

