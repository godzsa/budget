'use strict';
import {CategoryService} from "./category";
import {ServerService} from "./server";
import {ServerServiceMock} from "./server.mock";
import {Observable} from "rxjs/Observable";
import {Category} from "../../persistence/category";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

describe('Category Service', () => {
  let category: CategoryService;
  let server: ServerService = new ServerServiceMock();

  beforeEach(function () {
    category = new CategoryService(server);
  });

  it('should have basic categories', function () {
    expect(CategoryService.basicCategories.length).toBeGreaterThan(0);
  });

  it('should return basic categories', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of([]));
    let length: number = CategoryService.basicCategories.length;
    category.categories().subscribe(
      categories => {
        expect(categories.length).toEqual(length);
        done();
      }
    )
  });

  it('should return basic categories and concat the ones which come from server', function (done) {
    spyOn(server, 'request').and.returnValue(
      Observable.of([{icon: 'dummy', color: 'dummy'}, {icon: 'dummy', color: 'dummy'}])
    );
    let length: number = CategoryService.basicCategories.length;
    category.categories().subscribe(
      categories => {
        expect(categories.length).toEqual(length + 2);
        done();
      }
    )
  });

  it('should return basic categories if error occurs', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    let length: number = CategoryService.basicCategories.length;
    category.categories().subscribe(
      categories => {
        expect(categories.length).toEqual(length);
        done();
      }
    )
  });

  it('should return create category and push to server', function (done) {
    spyOn(server, 'request').and.returnValues(Observable.of('success'),
      Observable.of([{icon: 'dummy', color: 'dummy'}]));
    let length: number = CategoryService.basicCategories.length;

    category.create(new Category({})).subscribe(
      success => {
        category.categories().subscribe(
          categories => {
            expect(categories.length).toEqual(length + 1);
            done();
          });
      }
    );
  });

  it('should return create category and push to server', function (done) {
      let categ = {icon: 'dummy', color: 'dummy'};
      spyOn(server, 'request').and.returnValues(
        Observable.of('success'),
        Observable.of([categ]),
        Observable.of('success'),
        Observable.of([])
      );
      let length: number = CategoryService.basicCategories.length;

      category.create(new Category({})).subscribe(
        success => {
          category.categories().subscribe(
            categories => {
              expect(categories.length).toEqual(length + 1);

              category.remove(new Category(categ)).subscribe(
                success => {
                  category.categories().subscribe(
                    categories => {
                      expect(categories.length).toEqual(length);
                      done();
                    });
                }
              );
            });
        });
    }
  );
});

