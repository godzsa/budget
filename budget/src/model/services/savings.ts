'use strict';
import {Injectable} from "@angular/core";
import {ServerService} from "./server";
import {Method, Routes} from "../utils/router";
import {DateUtils} from "../utils/date";
import {Observable} from "rxjs";
import {Saving, SavingCategory, SavingInfo} from "../../persistence/saving";
/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */
@Injectable()
export class SavingsService {
  constructor(private serverService: ServerService) {
  }

  getDataForNew(): Observable<Saving> {
    return this.serverService.request(Routes.SAVINGS_NEW, Method.GET)
      .map(savingData => this.createSavingFromData(savingData))
  }

  getSavings(): Observable<Saving[]> {
    return this.serverService.request(Routes.SAVINGS, Method.GET)
      .map(savings => savings.map(saving => SavingsService.calcSaving(new Saving(saving))));
  }

  create(saving: Saving): Observable<any> {
    return this.serverService.request(Routes.SAVINGS, Method.CREATE, true, {saving});
  }

  remove(saving: Saving): Observable<any> {
    return this.serverService.request(Routes.SAVINGS, Method.REMOVE, true, {remove: saving._id});
  }

  private createSavingFromData(savingData: SavingCategory[]): Saving {
    let categories = savingData.map(category => new SavingCategory(category));
    return new Saving({categories});
  }

  static calcSaving(saving: Saving): Saving {
    saving.info = new SavingInfo({});
    saving.categories.map(category => SavingsService.calcCategoryStats(saving, category));
    return saving;
  }

  private static calcCategoryStats(saving: Saving, category: SavingCategory): SavingCategory {
    let progress = DateUtils.whereInProgress(saving.start, saving.end);
    category.info = this.createCategoryInfo(category, saving, progress);
    saving.info.add(category.info);

    return category;
  }

  private static createCategoryInfo(category: SavingCategory, saving: Saving, progress: number): SavingInfo {
    let spent = category.spent;
    let normalTarget = category.avg * DateUtils.dateDifference('month', saving.start, saving.end);
    let target = normalTarget * category.percent / 100;
    let optimal = target * progress;
    let saved = -(normalTarget * progress - spent);
    return new SavingInfo({spent, normalTarget, target, optimal, saved});
  }
}
