'use strict';
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ServerService} from "./server";
import {Method, Routes} from "../utils/router";
import {User} from "../../persistence/user";
import * as moment from "moment";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

@Injectable()
export class StatisticsService {
  constructor(private serverService: ServerService) {
  }

  getPieChartData(period: string, userOrGroup?: (User | string)): Observable<any> {
    let route = userOrGroup === 'group' ? Routes.STATISTICS_PIE_GROUP : Routes.STATISTICS_PIE;
    let data: any = {period: period};
    if (userOrGroup instanceof User) {
      data.user = userOrGroup._id;
    }

    return this.serverService.request(route, Method.POST, true, data)
      .map(data => data.map(this.transformToPie))
      .catch(err => Observable.throw(err));
  }

  private transformToPie(interval): any {
    let pie = {
      income: {colors: [], data: [], labels: []},
      expense: {colors: [], data: [], labels: []}
    };
    let expenseSum = 0;
    interval.categories.map(cat => {
      if (cat.sum > 0) {
        StatisticsService.addFormatedCategory(pie.income, cat);
      } else {
        StatisticsService.addFormatedCategory(pie.expense, cat);
        expenseSum += cat.sum;
      }
    });
    StatisticsService.addFormatedCategory(pie.income, {
      category: {name: 'expense', color: 'tomato'},
      sum: expenseSum
    });
    pie.expense.colors = [{backgroundColor: pie.expense.colors}];
    pie.income.colors = [{backgroundColor: pie.income.colors}];

    if (pie.expense.data.length === 0) {
      pie.expense = {colors: ["#aaa"], data: [0], labels: ["-"]};
    }
    if (pie.income.data.length === 0) {
      pie.income = {colors: ["#aaa"], data: [0], labels: ["-"]};
    }
    return pie;
  }

  static addFormatedCategory(array, cat) {
    array.colors.push(cat.category.color);
    array.data.push(cat.sum);
    array.labels.push(cat.category.name);
  }

  getLineChartData(period: any, userOrGroup: (User | string)) {
    let route = userOrGroup === 'group' ? Routes.STATISTICS_LINE_GROUP : Routes.STATISTICS_LINE;
    let data: any = {period: period};
    if (userOrGroup instanceof User) {
      data.user = userOrGroup._id;
    }

    let lastSum = 0;
    return this.serverService.request(route, Method.POST, true, data)
      .map(data => data.map(transactions => {
        let line = this.transformLine(transactions, lastSum);
        lastSum = line.nextSum;
        return line;
      }).reverse())
      .catch(err => Observable.throw(err));
  }

  //TODO: transformers to separate class
  private transformLine(data, lastInterval) {
    let last = lastInterval;
    let line = {datasets: [{data: [], label: 'line'}], labels: [], start: data.transactions[0].date, nextSum: 0};
    data.transactions.map((item) => {
      last += item.amount;
      line.datasets[0].data.push(last);
      line.labels.push(moment(item.date).format('Do MMM'));
    });
    line.nextSum = last;
    return line;
  }
}
