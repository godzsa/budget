'use strict';
import {Headers, Http} from "@angular/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Method, Routes, ServerData} from "../utils/router";

/**
 *
 * This ServerService should be used for every http request
 * It is an abstraction layer which uses predefined routes and request types,
 * It can send authenticated requests easily
 *
 */
@Injectable()
export class ServerService {
  public auth: { headers: Headers };

  constructor(private http: Http) {
    this.auth = {headers: new Headers()};
  }

  setAuthHeader(token: string) {
    this.auth.headers.append('Authorization', 'JWT ' + token);
  }

  request(page: Routes, method: Method, authenticated: boolean = true, data?: any): Observable<any> {
    let auth = authenticated ? this.auth : undefined;
    let request;
    page = ServerData.prefix + page;

    switch (method) {
      case Method.GET:
        request = this.http.get(page as string, auth);
        break;
      case Method.POST :
      case Method.CREATE :
        request = this.http.post(page as string, data, auth);
        break;
      case Method.EDIT :
        request = this.http.put(page as string, data, auth);
        break;
      case Method.REMOVE :
        request = this.http.delete(page + "/" + data.remove as string, auth);
        break;
      default:
        return Observable.throw(new Error("No such method"))
    }
    return request.map(res => this.getData(res));
  }

  private getData(res) {
    let data = res.json().data;
    if (data == null) {
      return res.json().success;
    }
    // data.status = res.status;
    return data
  }

  checkConnection(): Observable<boolean> {
    return this.request(Routes.PING, Method.GET)
      .catch(err => Observable.of(false));
  }
}


