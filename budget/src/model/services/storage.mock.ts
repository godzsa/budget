'use strict';
import {StorageService} from "./storage";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

export class StorageServiceMock extends StorageService {


  whenReady(fn: Function): Promise<any> {
    return Promise.resolve(fn());
  }

  push(name: string, data: any): Promise<any> {
    return Promise.resolve(true);
  }
}
