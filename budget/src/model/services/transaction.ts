import {Events} from "ionic-angular";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/concatMap";
import {Method, Routes} from "../utils/router";
import {Transaction} from "../../persistence/transaction";
import {User} from "../../persistence/user";
import {RepeatingTransaction} from "../../persistence/repeating";
import {SyncService} from "./sync";

@Injectable()
export class TransactionService {
  constructor(private syncService: SyncService, private events: Events) {
    this.update();
  }

  update():Promise<any>{
    return this.syncService.tryToSync()
      .then(() => this.events.publish('transaction:synced'))
      .catch(() => this.events.publish('offline', {message: 'You are offline'}));
  }

  nthBatch(nth: number): Observable<Transaction[]> {
    return this.getTransactions(Routes.TRANSACTION_BATCH, {batch: nth});
  }

  nthBatchForUser(nth: number, user: User): Observable<Transaction[]> {
    return this.getTransactions(Routes.TRANSACTION_BATCH, {batch: nth, user});
  }

  nthBatchGroup(nth: number): Observable<Transaction[]> {
    return this.getTransactions(Routes.TRANSACTION_GROUP_BATCH, {batch: nth});
  }

  repeating(): Observable<RepeatingTransaction[]> {
    return this.getTransactions(Routes.REPEATING_TRANSACTION_GET, undefined);
  }

  private getTransactions(route: Routes, data: any): Observable<Transaction[] | RepeatingTransaction[]> {
    return this.syncService.getData(route, data)
      .catch(TransactionService.handleError);
  }

  create(transaction: Transaction | RepeatingTransaction): Observable<any> {
    let isRepeating = transaction instanceof RepeatingTransaction;
    return this.syncService.persist(Method.CREATE, {transaction, isRepeating});
  }

  remove(transaction: Transaction | RepeatingTransaction): Observable<any> {
    let isRepeating = transaction instanceof RepeatingTransaction;
    return this.syncService.persist(Method.REMOVE, {remove: transaction._id || transaction.budgetId, isRepeating});
  }

  edit(transaction: Transaction | RepeatingTransaction): Observable<any> {
    let isRepeating = transaction instanceof RepeatingTransaction;
    let data = Object.assign(transaction, {updatedAt: new Date()});
    return this.syncService.persist(Method.EDIT, {transaction: data, isRepeating});
  }

  private static handleError(error): Observable<any> { //TODO: to errors util
    return Observable.throw(error);
  }
}
