'use strict';
import {ServerService} from "./server";
import {ServerServiceMock} from "./server.mock";
import {GroupService} from "./group";
import {Observable} from "rxjs/Observable";
import {User} from "../../persistence/user";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

describe('Group Service', function () {
  let server: ServerService = new ServerServiceMock();
  let group: GroupService;

  beforeEach(function () {
    group = new GroupService(server);
  });

  it('should return the group', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of(true));
    group.getGroup().subscribe(
      transformed => {
        expect(transformed).toBeTruthy();
        done();
      }
    )
  });

  it('should let the error through', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    group.getGroup().subscribe(
      success => {
        throw new Error('shouldnt be here')
      },
      error => {
        expect(error).toBeTruthy();
        done();
      });
  });

  it('should invite a user', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of(true));
    group.invite(new User({})).subscribe(
      transformed => {
        expect(transformed).toBeTruthy();
        done();
      }
    )
  });

  it('should let the error through', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    group.invite(new User({})).subscribe(
      success => {
        throw new Error('shouldnt be here');
      },
      error => {
        expect(error).toBeTruthy();
        done();
      });
  });

  it('should revoke invitation from user', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of(true));
    group.revokeInvitation(new User({})).subscribe(
      transformed => {
        expect(transformed).toBeTruthy();
        done();
      }
    )
  });

  it('should let the error through', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    group.revokeInvitation(new User({})).subscribe(
      success => {
        throw new Error('shouldnt be here')
      },
      error => {
        expect(error).toBeTruthy();
        done();
      });
  });

  it('should create group', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of(true));
    group.createGroup().subscribe(
      transformed => {
        expect(transformed).toBeTruthy();
        done();
      }
    )
  });

  it('should let the error through', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    group.createGroup().subscribe(
      success => {
        throw new Error('shouldnt be here')
      },
      error => {
        expect(error).toBeTruthy();
        done();
      });
  });

  it('should let user join group', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of(true));
    group.joinGroup().subscribe(
      transformed => {
        expect(transformed).toBeTruthy();
        done();
      }
    )
  });

  it('should let the error through', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    group.joinGroup().subscribe(
      success => {
        throw new Error('shouldnt be here')
      },
      error => {
        expect(error).toBeTruthy();
        done();
      });
  });

  it('should let user join group', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.of(true));
    group.leaveGroup().subscribe(
      transformed => {
        expect(transformed).toBeTruthy();
        done();
      }
    )
  });

  it('should let the error through', function (done) {
    spyOn(server, 'request').and.returnValue(Observable.throw(new Error('error')));
    group.leaveGroup().subscribe(
      success => {
        throw new Error('shouldnt be here')
      },
      error => {
        expect(error).toBeTruthy();
        done();
      });
  });

});
