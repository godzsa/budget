'use strict';
import {SavingsService} from "./savings";
import {ServerServiceMock} from "./server.mock";
import {ServerService} from "./server";
import {Saving, SavingCategory, SavingInfo} from "../../persistence/saving";
import {Observable} from "rxjs/Observable";
import * as moment from "moment";
import * as _ from "lodash";

import {DateUtils} from "../utils/date";

/**
 * @author Godzsák Dávid <godzsakdavid@gmail.com>
 */

describe('Savings Service', function () {
  let server: ServerService = new ServerServiceMock();
  let savings: SavingsService;

  beforeEach(function () {
    savings = new SavingsService(server);
  });

  it('should transform the data from the request for new saving', function (done) {
    let baseSavingData = [
      {category: {color: 'dummy1', icon: 'dummy1'}, avg: 1234},
      {category: {color: 'dummy2', icon: 'dummy2'}, avg: 431}
    ];

    let result = new Saving({
      categories: [
        new SavingCategory(baseSavingData[0]),
        new SavingCategory(baseSavingData[1])
      ]
    });

    spyOn(server, 'request').and.returnValue(Observable.of(baseSavingData));

    savings.getDataForNew().subscribe(
      transformed => {
        //jasmine equality check is not working deeply here
        expect(_.isEqual(result,transformed)).toEqual(true);
        done();
      }
    )
  });

  it('should return the saving', function (done) {
    let saving = new Saving({});
    spyOn(server, 'request').and.returnValue(Observable.of(true));


    savings.create(saving).subscribe(
      transformed => {
        expect(transformed).toEqual(true);
        done();
      }
    )
  });

  it('should remove the saving', function (done) {
    let saving = new Saving({});
    spyOn(server, 'request').and.returnValue(Observable.of(true));

    savings.remove(saving).subscribe(
      transformed => {
        expect(transformed).toEqual(true);
        done();
      }
    )
  });

  //TODO: more tests
  it('should calculate the savings so far, and provide user with data', function () {
    spyOn(DateUtils, 'whereInProgress').and.returnValue(0.4);

    let name = 'name';
    let start = moment().subtract(1, 'month').startOf('day').toDate();
    let end = moment().add(1, 'month').startOf('day').toDate();
    let categories = [
      new SavingCategory({category: {color: 'dummy1', icon: 'dummy1'}, avg: -1000, spent: -1500, percent: 60}),
      new SavingCategory({category: {color: 'dummy2', icon: 'dummy2'}, avg: -2000, spent: -500, percent: 80})
    ];

    let saving = new Saving({name, start, end, categories});

    let result = new Saving({
      name,
      start,
      end,
      categories: [
        new SavingCategory({
          category: categories[0].category,
          avg: -1000,
          spent: -1500,
          percent: 60,
          info: new SavingInfo({
            spent: -1500,
            saved: -700,
            optimal: -480,
            target: -1200,
            normalTarget: -2000
          })
        }),
        new SavingCategory({
          category: categories[1].category,
          avg: -2000,
          spent: -500,
          percent: 80,
          info: new SavingInfo({
            spent: -500,
            saved: 1100,
            optimal: -1280,
            target: -3200,
            normalTarget: -4000
          })
        })
      ],
      info: new SavingInfo({
        spent: -2000,
        saved: 400,
        optimal: -1760,
        target: -4400,
        normalTarget: -6000
      })
    });

    expect(_.isEqual(result,SavingsService.calcSaving(saving))).toEqual(true);
  });

});
