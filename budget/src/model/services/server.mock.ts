'use strict';
import {Observable} from "rxjs";
import {Method} from "../utils/router";
import {ServerService} from "./server";
import {Http, RequestOptions} from "@angular/http";
import {MockBackend} from "@angular/http/testing";

/**
 * Created by david on 2016.12.27..
 */


export class ServerServiceMock extends ServerService {
  constructor() {
    super(new Http(new MockBackend(), new RequestOptions()))
  }

  setAuthHeader(token: string) {
    //
  }

  request(page: string, method: Method, authenticated: boolean = true, data?: any): Observable<any> {
    return Observable.fromPromise(Promise.resolve(true));
  }

  checkConnection(): Observable<boolean> {
    return Observable.of(true);
  }

}


