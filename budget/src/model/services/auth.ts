'use strict';
/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.27..
 */
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {User} from "../../persistence/user";
import {Method, Routes} from "../utils/router";
import {StorageService} from "./storage";
import {ServerService} from "./server";
import {Events} from "ionic-angular";

@Injectable()
export class AuthService {
  public user: User;

  constructor(private storageService: StorageService, private serverService: ServerService, public events: Events) {
    this.user = new User({});
  }

  isLoggedIn(): boolean {
    return this.user && this.user.token && this.user.token !== "";
  }

  login(user?: User): Observable<boolean> {
    if (user == null) {
      return this.loginWithStoredToken();
    } else {
      return this.loginWithServer(user);
    }
  }

  register(newUser: User): Observable<boolean> {
    return this.serverService.request(Routes.REGISTER, Method.CREATE, false, {user: newUser})
      .map(data => this.setUser(newUser, data.token))
      .catch(err => this.handleError(err));
  }

  logout() {
    this.user = null;
    this.storageService.ready().then(() => this.storageService.remove(StorageService.USER));
  }

  private loginWithStoredToken(): Observable<boolean> { //TODO: refactor to async/await
    return Observable.fromPromise(this.storageService.whenReady(() =>
      this.storageService.get(StorageService.USER)))
      .map(storedUser => {
        if (storedUser == null) {
          throw new Error("Please log in");
        }
        this.setUser(storedUser, storedUser.token)
      })
      .catch(err => this.handleError(err));

  }

  private loginWithServer(user: User): Observable<boolean> {
    return this.serverService.request(Routes.LOGIN, Method.POST, false, {user: user})
      .map(data => this.setUser(user, data.token))
      .catch(err => this.handleError(err));
  }

  private setUser(user: User, token: string): boolean {
    if (token == null) {
      this.events.publish('logout', user, 'Token not found!');
      return false;
    }

    this.serverService.setAuthHeader(token);

    this.user = user;
    this.user.token = token;
    this.user.password = "-";
    this.storageService.ready().then(
      () => this.storageService.set(StorageService.USER, this.user).catch(err => this.handleError(err))
    );
    return true;
  }

  private handleError(error): Observable<any> {
    if (error.status === 400) {
      this.events.publish('error', {message: 'Invalid data!'});
    } else {
      this.events.publish('error', error);
    }
    return Observable.throw(error);
  }
}
