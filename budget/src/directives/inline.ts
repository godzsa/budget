'use strict';
/**
 * Created by david on 2017.02.05..
 */
import {Directive, ElementRef} from "@angular/core";

@Directive({selector: '[inline]'})
export class InlineDirective {
  constructor(el: ElementRef) {
    el.nativeElement.style.display = 'inline-block';
  }
}
