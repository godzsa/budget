/**
 * Created by David Godzsak <godzsakdavid@gmail.com> on 2016.12.12.
 */
"use strict";

const fs = require('fs');
const gulp = require('gulp');
const del = require('del');
const merge = require('merge-stream');
const cache = require('gulp-cached');
const open = require('gulp-open');
const fork = require('child_process').fork;
const argv = require('yargs').argv;

//Builders
const BUILDER = {
  ionic: 'node_modules/.bin/ionic-app-scripts',
  cordova: 'node_modules/.bin/cordova',
  electron: 'node_modules/.bin/electron',
  electronPackager: 'node_modules/.bin/electron-packager',
  angular: 'node_modules/.bin/ng'
};

//Platforms
const IONIC = {
  android: "android",
  ios: "ios",
  wp: "windows",

};
const ELECTRON = {
  windows: "win32",
  linux: "linux",
  mac: "darwin",
  all: "all"
};

const isRelease = argv._.includes("dist");
//ARGS
const builder = argv.builder || argv.b;
const view = argv.view || argv.v;
const platform = argv.platform || argv.p;
const isLab = argv.lab || argv.l;


let isIonic = view === 'ionic';
let isElectron = view === 'electron';
exitIfInvalid();

//CLEAN
gulp.task('clean', () => {
  del.sync('.tmp');
  del.sync('www/');
  fs.mkdirSync('www/');
});


//COPY
function viewFiles() {
  return gulp.src('src/' + view + '_view/**/*', {base: "src/" + view + "_view"})
    .pipe(cache('copy'))
    .pipe(gulp.dest('.tmp/view/'));

}

function copy() {
  let common = gulp.src(['!src/**_view{/**,}', 'src/**/*'], {base: "src"})
    .pipe(cache('copy'))
    .pipe(gulp.dest('.tmp/'));

  return merge(viewFiles(), common);
}

gulp.task('copy', ['clean'], () => copy());
gulp.task('copy:noclean', () => copy());


//WATCH
gulp.task('watch', ['copy'], () => gulp.watch(['src/**/*.*'], ['copy:noclean']));


//TEST
gulp.task('test', ['copy'], (cb) => runBuilder(BUILDER.angular, 'test', [], cb));

//NPM TASKS (build, serve, run, dist)
//TODO: refactor nasty callbacks to Promise maybe?
gulp.task('build', ['copy'], (cb) => {
  let releaseParam = isRelease ? ['--aot', '--prod', '--release'] : [];

  runBuilder(BUILDER.ionic, 'build', releaseParam, cb)
});

gulp.task('serve', ['watch'], (cb) => {
  let labParam = isLab ? ['--lab'] : [];

  runBuilder(BUILDER.ionic, 'serve', labParam, cb);
});

gulp.task('run', ['build'], (cb) => {
  if (isIonic) {
    runBuilder(BUILDER.cordova, 'run', [IONIC[platform], '--debug', '--emulator'], cb);
  } else {
    runBuilder(BUILDER.electron, ".", [], cb);
  }
});

gulp.task('dist', ['build'], (cb) => {
  if (isIonic) {
    runBuilder(BUILDER.cordova, 'build', [IONIC[platform], '--release', '--prod', '--platform'], cb);
  } else {
    runBuilder(BUILDER.electronPackager, './www', ['Budget', '--platform=' + ELECTRON[platform], '--arch=all', '--out=platforms', '--asar', '--overwrite'], cb)
  }
});


function runBuilder(builder, task, args = [], cb) {
  let argList = [task].concat(args);
  let child = fork(builder, argList);

  child.on('close', (err) => {
    if (err) return cb(err);

    if (cb) {
      cb();
    }
  });
}

function error(msg) {
  console.log("ERROR: " + msg);
  process.exit();
}


function exitIfInvalid() {
  if (argv._.includes("test")) {
    return;
  }

  if (!(isIonic || isElectron)) { //TODO: refactor
    error('Use -v=ionic or -v=electron for given platform.');
  }
  if (isIonic && isElectron) {
    error('Ambiguous platform definition');
  }
  if (isRelease) {
    if (!(IONIC.hasOwnProperty(platform) || ELECTRON.hasOwnProperty(platform))) {
      error('Please define a platform with -p or --platform (Ionic: android, ios, wp. Electron: mac, linux, windows)');
    }
    if (isIonic && !IONIC.hasOwnProperty(platform)) {
      error('Ionic builds the following platforms: android, ios, wp');
    }
    if (isElectron && !ELECTRON.hasOwnProperty(platform)) {
      error('Electron builds the following platforms: mac, linux, windows, all');
    }
  }
}
